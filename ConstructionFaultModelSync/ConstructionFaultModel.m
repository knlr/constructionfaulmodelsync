//
//  ConstructionFaultModel.m
//  ConstructionFaultModelSync
//
//  Created by Knut Lorenzen on 22/04/2013.
//  Copyright (c) 2013 Selling Sollutions. All rights reserved.
//
// - views in Couch are a bit like indexes, except they are essential to do anything meaningful
//
// - they are incrementally rebuilt everytime a document is updated
//
// - map/reduce functions MUST be side effect free, i.e. only operate on the parameters passed
//
// - reduce is optional and useful for computing sums or counts (I haven't found a case where I need that yet)
//
// - in TouchDB (unlike CouchDB), the map/reduce functions cannot be stored inside the database since they are
//   Objective-C blocks rather than Javascript, and need to be loaded at launch time.
//
// - the views they generate are persistent though
//
// - before querying a view, it MUST be initialized using defineViewNamed after every launch
//
// - a view will not be rebuilt unless the passed version string differs from the last call, so it's
//   convenient to always call defineViewNamed right before queryViewNamed
//
// - I found that TouchDB rebuilds a view only when a query is run on it and it's outdated (i.e. the database has
//   changed since it was last updated)
//


#import "ConstructionFaultModel.h"
#import "ConstructionError.h"

#pragma mark private declacartions

@interface CouchDocument(RevisionTree)
-(BOOL)determineLosingHead:(CouchRevision**)losingHead AndCommonAncestor:(CouchRevision**)ancestor;
@end

@interface CouchRevision(RevisionTree)
-(NSUInteger)sequenceNumber;
@end


@interface ConstructionFaultModel ()
-(void)setValidationBlock;

@property CouchLiveQuery* conflictQuery;

@end


@implementation ConstructionFaultModel{
    CouchDesignDocument* _designDoc;
    ConstructionError* _currentError;
}

static ConstructionFaultModel* __defaultInstance = nil;
+(ConstructionFaultModel*)default
{
    if (!__defaultInstance)
        __defaultInstance = [[ConstructionFaultModel alloc] init];
    return __defaultInstance;
}



#pragma mark ConstructionFaultModel implementation


-(id)init
{
    if ( !(self = [super init]) )
        return nil;

    CouchTouchDBServer* server = [CouchTouchDBServer sharedInstance];
    CouchDatabase* database = nil;
    NSError* error = server.error;
    BOOL dbInitialized = NO;
    if ( !error ) {
        database = [server databaseNamed: @"construction_faults"];  // db name must be lowercase!
        dbInitialized = [database ensureCreated:&error];
    }
    if ( !dbInitialized ) {
        NSLog( @"%@", error );
        return nil;
    }
    // CouchDesignDocument is CouchCocoa's main interface and holds a reference to the CouchDatabase,
    // so it's only necessary to store a reference to the design doc
    _designDoc = [database designDocumentWithName:@"iOS"];
    _currentError = [ConstructionError default];
    
    [self setValidationBlock];
    
    // we wan't to monitor the database for occurring conflicts. This is done by defining a view, runing a
    // CouchLiveQuery on it and observing the property 'rows'. Every time the view is updated, observeValueForKeyPath
    // gets called
    [self.designDoc defineViewNamed:@"conflicts" mapBlock:MAPBLOCK(
    {
       if ( doc[@"_conflicts"] ){
           emit( doc[@"_id"], doc[@"_conflicts"] );
       }
    }) version:@"1"];
    self.conflictQuery = [[self.designDoc queryViewNamed:@"conflicts"] asLiveQuery];
    self.conflictQuery.prefetch = YES;
    [self.conflictQuery addObserver:self forKeyPath:@"rows" options:0 context:NULL];
    [self.conflictQuery start];
    return self;    
}    


#pragma mark queries
-(CouchLiveQuery*)querySpaces
{
    [self.designDoc defineViewNamed: @"all_spaces" mapBlock: MAPBLOCK({
        if ( [doc[@"type"] isEqual:@"space"] )
            emit( doc[@"name"], doc[@"name"]);
    }) version:@"1"];
    return [self.designDoc queryViewNamed:@"all_spaces"].asLiveQuery;
}

-(CouchLiveQuery*)queryUnits
{
    [self.designDoc defineViewNamed:@"units_by_space" mapBlock:MAPBLOCK(
    {
        if ( [(NSString*)doc[@"type"] isEqualToString:@"unit"] )
            for ( NSString* spaceId in doc[@"spaces"] )
                // unlike CouchDB, TouchDB does not sort the view results by doc.id
                // this is a little workaround that will sort the displayed units alphabetically despite:
                // we append the name to the emmited key to get sorting right and later query with start and end key
                // instead of a discinct key
                emit( @[spaceId, doc[@"name"]],  doc[@"name"] );
    }) version:@"1"];
    CouchQuery* q = [self.designDoc queryViewNamed:@"units_by_space"];
    NSAssert( _currentError.space.length > 0, @"" );
    // this view is an example showing a way to sort the results alphabetically. The key consists of
    // the space's id and the unit's name
    // In order to catch all the rows with a specific space id, we use start and end key as follows:
    q.startKey = @[_currentError.space];
    q.endKey = @[_currentError.space, @"\ufff0"];    
    // a high unicode value as endkey guarantees that all strings are included
    // see also: http://wiki.apache.org/couchdb/View_collation?action=show&redirect=ViewCollation#String_Ranges
    // (the other views are kept simpler, but may not be sorted alphabetically)
    return q.asLiveQuery;
}



-(CouchLiveQuery*)queryObjects
{
    NSAssert( _currentError.space, @"" );
    CouchQuery* q = nil;
    if ( !_currentError.unit ){
        [self.designDoc defineViewNamed:@"objects_by_space" mapBlock:MAPBLOCK(
        {
          if ( [((NSString*)doc[@"type"]) isEqualToString:@"object"] )
              for ( NSString* spaceId in doc[@"spaces"] )
                  emit( spaceId, doc[@"name"] );
        }) version:@"1"];
    }
    else {
        [self.designDoc defineViewNamed:@"objects_by_space_and_unit" mapBlock:MAPBLOCK(
        {
           if ( [(NSString*)doc[@"type"] isEqualToString:@"object"] )
           {
               NSDictionary* spacesUnits = doc[@"spaces_units"];
               for ( NSString* spaceId in spacesUnits.allKeys )
                   for ( NSString* unitId in (NSArray*)(spacesUnits[spaceId]))
                       emit( @[spaceId, unitId], doc[@"name"] );
           }
        }) version:@"1"];
        q = [self.designDoc queryViewNamed:@"objects_by_space_and_unit"];
        q.keys = @[@[_currentError.space, _currentError.unit]];
    }
    return q.asLiveQuery;
}


-(CouchLiveQuery*)queryDetails
{
    CouchQuery* q = nil;    
    NSAssert( _currentError.space, @"" );
    
    if (  _currentError.unit &&
         !_currentError.object )
    {
        #pragma mark details_by_space_unit
        [self.designDoc defineViewNamed:@"details_by_space_unit" mapBlock:MAPBLOCK({
            if ( [(NSString*)doc[@"type"] isEqualToString:@"detail"] )
            {
                NSDictionary* spacesUnits = doc[@"spaces_units"];
                for ( NSString* spaceId in spacesUnits.allKeys )
                    for ( NSString* unitId in (NSArray*)(spacesUnits[spaceId]))
                        emit( @[spaceId, unitId], doc[@"name"] );
            }
        }) version:@"1"];
        q = [self.designDoc queryViewNamed:@"details_by_space_unit"];
        q.keys = @[@[_currentError.space, _currentError.unit]];
    } else
    if (  !_currentError.unit &&
          _currentError.object )
    {
        #pragma mark details_by_space_object                
        [self.designDoc defineViewNamed:@"details_by_space_object" mapBlock:MAPBLOCK({
            if ( [(NSString*)doc[@"type"] isEqualToString:@"detail"] )
            {
                NSDictionary* spacesObjects = doc[@"spaces_objects"];
                for ( NSString* spaceId in spacesObjects.allKeys )
                    for ( NSString* objectId in (NSArray*)(spacesObjects[spaceId]))
                        emit( @[spaceId, objectId], doc[@"name"] );
            }
        }) version:@"1"];
        q = [self.designDoc queryViewNamed:@"details_by_space_object"];
        q.keys = @[@[_currentError.space, _currentError.object]];
    } else
    if (  _currentError.unit &&
          _currentError.object )
    {
        #pragma mark   details_by_space_unit_object        
        [self.designDoc defineViewNamed:@"details_by_space_unit_object" mapBlock:MAPBLOCK({
            if ( [(NSString*)doc[@"type"] isEqualToString:@"detail"] )
            {
                NSDictionary* spacesUnitsObjects = doc[@"spaces_units_objects"];
                for ( NSString* spaceId in spacesUnitsObjects.allKeys )
                {
                    NSDictionary* unitsObjects = spacesUnitsObjects[spaceId];
                    for (NSString* unitId in unitsObjects.allKeys ) {
                        for ( NSString* objectId in (NSArray*)(unitsObjects[unitId]) )
                            emit( @[spaceId, unitId, objectId], doc[@"name"] );
                    }
                }
            }
        }) version:@"1"];
        q = [self.designDoc queryViewNamed:@"details_by_space_unit_object"];
        q.keys = @[@[_currentError.space, _currentError.unit, _currentError.object]];
    } else
    if (  !_currentError.unit &&
          !_currentError.object )
    {
        q = self.designDoc.database.getAllDocuments;
        q.keys = @[];
    }
    
    return q.asLiveQuery;
}

-(CouchLiveQuery*)queryErrorCodes
{
    CouchQuery* q = nil;
    NSAssert( _currentError.space, @"" );
    NSMutableArray* key = [NSMutableArray arrayWithObject:_currentError.space];
    
    if ( !_currentError.detail )
    {
        if ( !_currentError.unit &&
             !_currentError.object )
        {
            #pragma mark error_codes_by_space
            [self.designDoc defineViewNamed:@"error_codes_by_space" mapBlock:MAPBLOCK({
                if ( [(NSString*)doc[@"type"] isEqualToString:@"error_code"] )
                    for ( NSString* spaceId in doc[@"spaces"] ) {
                        emit( spaceId, doc[@"name"]);
                    }
            }) version:@"1"];
            q = [self.designDoc queryViewNamed:@"objects_by_space"];
        } else    
        if (  _currentError.unit &&
             !_currentError.object  )
        {
            #pragma mark error_codes_by_space_unit            
            [self.designDoc defineViewNamed:@"error_codes_by_space_unit" mapBlock:MAPBLOCK({
                if ( [(NSString*)doc[@"type"] isEqualToString:@"error_code"] )
                {
                    NSDictionary* spacesUnits = doc[@"spaces_units"];
                    for ( NSString* spaceId in spacesUnits.allKeys )
                        for ( NSString* unitId in (NSArray*)(spacesUnits[spaceId]))
                            emit( @[spaceId, unitId], doc[@"name"] );
                }
            }) version:@"1"];
            q = [self.designDoc queryViewNamed:@"error_codes_by_space_unit"];
            [key addObject:_currentError.unit];
        } else
        if ( !_currentError.unit &&
              _currentError.object )
        {
            #pragma mark error_codes_by_space_object            
            [self.designDoc defineViewNamed:@"error_codes_by_space_object" mapBlock:MAPBLOCK({
                if ( [(NSString*)doc[@"type"] isEqualToString:@"error_code"] )
                {
                    NSDictionary* spacesObjects = doc[@"spaces_objects"];
                    for ( NSString* spaceId in spacesObjects.allKeys )
                        for ( NSString* objectId in (NSArray*)(spacesObjects[spaceId]))
                            emit( @[spaceId, objectId], doc[@"name"] );
                }
            }) version:@"1"];
            q = [self.designDoc queryViewNamed:@"error_codes_by_space_object"];
            [key addObject:_currentError.object];
        } else
        if ( _currentError.unit &&
             _currentError.object )
        {
            #pragma mark error_codes_by_space_unit_object            
            [self.designDoc defineViewNamed:@"error_codes_by_space_unit_object" mapBlock:MAPBLOCK({
                if ( [(NSString*)doc[@"type"] isEqualToString:@"error_code"] )
                {
                    NSDictionary* spacesUnitsObjects = doc[@"spaces_units_objects"];
                    for ( NSString* spaceId in spacesUnitsObjects.allKeys )
                    {
                        NSDictionary* unitsObjects = spacesUnitsObjects[spaceId];
                        for (NSString* unitId in unitsObjects.allKeys ) {
                            for ( NSString* objectId in (NSArray*)(unitsObjects[unitId]) )
                                emit( @[spaceId, unitId, objectId], doc[@"name"] );
                        }
                    }
                }
            }) version:@"1"];
            q = [self.designDoc queryViewNamed:@"error_codes_by_space_unit_object"];        
            [key addObject:_currentError.unit];
            [key addObject:_currentError.object];        
        }
        else NSAssert( NO, @"" );
    }
    else
    {
        // detail selected                
        if ( _currentError.unit &&
            !_currentError.object )
        {
            #pragma mark error_codes_by_space_unit_detail
            [self.designDoc defineViewNamed:@"error_codes_by_space_unit_detail" mapBlock:MAPBLOCK({
                if ( [(NSString*)doc[@"type"] isEqualToString:@"error_code"] )
                {
                    NSDictionary* spacesUnitsDetails = doc[@"spaces_units_details"];
                    for ( NSString* spaceId in spacesUnitsDetails.allKeys )
                    {
                        NSDictionary* unitsDetails = spacesUnitsDetails[spaceId];
                        for ( NSString* unitId in unitsDetails.allKeys )
                            for ( NSString* detailId in (NSArray*)unitsDetails[unitId] )
                                emit( @[spaceId, unitId, detailId], doc[@"name"] );
                    }
                }
            }) version:@"1"];
            q = [self.designDoc queryViewNamed:@"error_codes_by_space_unit_detail"];
            [key addObject:_currentError.unit];
        }
        else
        if ( !_currentError.unit &&
             _currentError.object )
        {
            #pragma mark error_codes_by_space_object_detail
            [self.designDoc defineViewNamed:@"error_codes_by_space_object_detail" mapBlock:MAPBLOCK({
                if ( [(NSString*)doc[@"type"] isEqualToString:@"error_code"] )
                {
                    NSDictionary* spacesObjectsDetails = doc[@"spaces_objects_details"];
                    for ( NSString* spaceId in spacesObjectsDetails.allKeys )
                    {
                        NSDictionary* objectsDetails = spacesObjectsDetails[spaceId];
                        for (NSString* objectId in objectsDetails.allKeys ) {
                            for ( NSString* detailId in (NSArray*)(objectsDetails[objectId]) )
                                emit( @[spaceId, objectId, detailId], doc[@"name"] );
                        }
                    }
                }
            }) version:@"1"];
            q = [self.designDoc queryViewNamed:@"error_codes_by_space_object_detail"];
            [key addObject:_currentError.object];
        }
        else
        if ( _currentError.unit &&
            _currentError.object )
        {
            #pragma mark error_codes_by_space_unit_object_detail
            [self.designDoc defineViewNamed:@"error_codes_by_space_unit_object_detail" mapBlock:MAPBLOCK({
                if ( [(NSString*)doc[@"type"] isEqualToString:@"error_code"] )
                {
                    NSDictionary* spacesUnitsObjectsDetails = doc[@"spaces_units_objects_details"];
                    for ( NSString* spaceId in spacesUnitsObjectsDetails.allKeys )
                    {
                        NSDictionary* unitsObjectsDetails = spacesUnitsObjectsDetails[spaceId];
                        for ( NSString* unitId in unitsObjectsDetails.allKeys )
                        {
                            NSDictionary* objectsDetails = unitsObjectsDetails[unitId];
                            for ( NSString* objectId in objectsDetails.allKeys )
                                for (NSString* detailId in (NSArray*)objectsDetails[objectId] )
                                    emit( @[spaceId, unitId, objectId, detailId], doc[@"name"] );
                        }
                    }
                }
            }) version:@"1"];
            q = [self.designDoc queryViewNamed:@"error_codes_by_space_unit_object_detail"];
            [key addObject:_currentError.unit];
            [key addObject:_currentError.object];            
        }
        else
        if ( !_currentError.unit &&
             !_currentError.object )            
            q = self.designDoc.database.getAllDocuments;

        [key addObject:_currentError.detail];
    }    
    q.keys = @[key];
    return q.asLiveQuery;
}

-(CouchLiveQuery*)queryLocations
{
    [self.designDoc defineViewNamed: @"locations" mapBlock: MAPBLOCK({
        if ( [doc[@"type"] isEqual:@"location"] )
            emit( nil, doc[@"name"] );
    }) version: @"1"];
    return [[self.designDoc queryViewNamed:@"locations"] asLiveQuery ];
}

#pragma mark validation


-(void)setValidationBlock
{
    // this is an example of what a validation function can look like. It's a way to protect yourself from
    // writing malformed documents to the db, a bit like a unit test
    [_designDoc setValidationBlock:^(TD_Revision* newRevision, id<TD_ValidationContext> context)
     {
         NSDictionary *props = [newRevision properties];
         
         // don't check design documents
         if ( [(NSString*)props[@"_id"] hasPrefix:@"_design/"] )
             return YES;
         
         // let deletions pass though, too
         if ( [props[@"_deleted"] boolValue] )
             return YES;

         
         NSString *docType = props[@"type"];
         // doc.type is mandatory
         if ( ![docType isKindOfClass:[NSString class]] )
         {
             [context setErrorMessage:@"doc.type missing"];
             NSLog(@"%@ validation error:%@", newRevision.docID, [context errorMessage] );
             return NO;
         }
         
         // allowed values for doc.type
         if ( ![@[@"space", @"unit", @"object", @"detail", @"error_code",
                @"responsible_party", @"location",
                @"construction_fault"] containsObject:docType] )
         {
             [context setErrorMessage:@"invalid doc.type"];
             NSLog(@"%@ validation error:%@", newRevision.docID, [context errorMessage] );
             return NO;
         }
         
         // all docs except construction_faults must have doc.name
         if ( ![props[@"name"] isKindOfClass:[NSString class]]  &&
             ![docType isEqual:@"construction_fault"] )
         {
             [context setErrorMessage:@"doc.name missing"];
             NSLog(@"%@ validation error:%@", newRevision.docID, [context errorMessage] );
             return NO;
         }
         
         for ( NSString* childType in  @[@"units", @"objects", @"details", @"error_codes"] )
         {
             NSArray* asArray = props[childType];
             if ( asArray != nil )
             {
                 // any of these fields must be of type array (if present)
                 if ( ![asArray isKindOfClass:[NSArray class]] )
                 {
                     [context setErrorMessage:[NSString stringWithFormat:@"%@ is of type %@, expected NSArray!",
                                               childType, [[asArray class] description] ]];
                     NSLog(@"%@ validation error:%@", newRevision.docID, [context errorMessage] );
                     return NO;
                 }
                 // duplicate references are not allowed
                 NSSet* asSet = [NSSet setWithArray:asArray];
                 if ( asSet.count != asArray.count )
                 {
                     [context setErrorMessage:[NSString stringWithFormat:@"%@ contains duplicates", childType]];
                     NSLog(@"%@ validation error:%@", newRevision.docID, [context errorMessage] );
                     return NO;
                 }
             }
         }
         return YES;
     }];
}



#pragma mark conflict management


// (see init) this gets called whenever TouchDB detects a conflict
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change
                      context:(void *)context
{
    
    if ( object == self.conflictQuery )
    {
        NSLog( @"conflicts view: ");
        NSMutableArray *conflictingDocs = [NSMutableArray array];
        
        CouchQueryEnumerator *e = self.conflictQuery.rows;
        CouchQueryRow *r;
        while ( ( r = e.nextRow ) )
        {
            NSLog( @"key: %@ value: %@ ", ((NSString*)r.key).debugDescription,
                  ((NSArray*)r.value).debugDescription );
            [conflictingDocs addObject:r.document];
        };
        if ( conflictingDocs.count )
        {
            [self mergeReferencesInDocuments:conflictingDocs];
        }
        return;
    }
   
    
}

// This is a stricly limited merging algorithm: It only applies to the field spaces inside a unit document.
// What it does is a three-way merge, i.e. it diffs the leaf version of the losing history line (read below on
// CouchDB MVCC and conflicts) against the common ancestor, and applies this diff to the current revison.
//
// Merging references inside an array can be done by an algorithm.
//
-(RESTOperation*)mergeReferencesInDocuments:(NSArray*)documents
{
    NSMutableArray *changes    = [NSMutableArray array],
                   *revisions  = [NSMutableArray array];
    
    for ( CouchDocument* document in documents )
    {
        // this merging algorithm is limited to unit documents
        // since it is the only conflict we anticipate
        if ( ![document[@"type"] isEqual:@"unit"] )
        {
            NSLog( @"%@: conflict will not be resolved (only units supported)",
                  document.documentID );
            continue;
        }
        CouchRevision  *losingHeadRev    = nil,
                       *ancestorRev      = nil,
                       *currentRev       = document.currentRevision;
        
        NSMutableDictionary* updatedDocProperties = nil;
        if ( ![document determineLosingHead:&losingHeadRev
                          AndCommonAncestor:&ancestorRev] ){
            NSLog( @"%@: could not determine conflicting head revision or common ancestor",
                  document.documentID );
        }
        else
        {
            updatedDocProperties =  [NSMutableDictionary dictionaryWithDictionary:document.properties];
            
            NSSet* winningRevSpaceIds = [NSSet setWithArray:currentRev[@"spaces"]];
            NSSet* losingRevSpaceIds  = [NSSet setWithArray:losingHeadRev[@"spaces"]];
            
            // do the conflicting revisions differ in this array?
            if ( ![winningRevSpaceIds isEqualToSet:losingRevSpaceIds] )
            {
                // diff the losing rev against its ancestor
                NSSet* parentRevSpaceIds = [NSSet setWithArray:ancestorRev[@"spaces"]];
                
                NSMutableSet* addedByLosingRev = [NSMutableSet setWithSet:losingRevSpaceIds] ;
                [addedByLosingRev minusSet:parentRevSpaceIds];
                
                NSMutableSet* removedByLosingRev = [NSMutableSet setWithSet:parentRevSpaceIds] ;
                [removedByLosingRev minusSet:losingRevSpaceIds];
                
                // apply the diff result to the winning rev
                NSMutableSet* mergedSpaceIds = [NSMutableSet setWithSet:winningRevSpaceIds];
                [mergedSpaceIds minusSet:removedByLosingRev];
                [mergedSpaceIds unionSet:addedByLosingRev];
                updatedDocProperties[@"spaces"] = [mergedSpaceIds allObjects];
                NSLog( @"merged doc.spaces of %@", document.documentID );
                
                [changes   addObject:updatedDocProperties];
                [revisions addObject:currentRev];
            }
            // all losing revisions must be deleted (by setting them to NSNull)
            for ( CouchRevision* rev in document.getConflictingRevisions )
            {
                if ( !rev.isCurrent )
                {
                    [changes   addObject:[NSNull null]];
                    [revisions addObject:rev];
                }
            }
        }
    }
    // CouchDocument has a convenience method for resolving conficts called
    // resolveConflictingRevisions:withProperties:
    // (http://labs.couchbase.com/CouchCocoa/docs/interfaceCouchDocument.html#ae245c650e1d0b55a6bab20fcc55877c3)
    // but it can only resolve one conflict at a time. To resolve resolve conflicts in multiple docs with a bulk
    // update CouchDesignDocument::putChanges:toRevisions: can be used instead
    return [[_designDoc.database putChanges:changes toRevisions:revisions] start];
}



@end


# pragma mark CouchRevision extension



@implementation CouchRevision(RevisionTree)

// The revision of a CouchDB document is comprised of two parts separated by a dash; a sequence number that reflects
// how often the document was updated, and an md5 hash over its contents.
// This method extracts that sequence number.

-(NSUInteger)sequenceNumber
{
    return [[NSDecimalNumber
             decimalNumberWithString:[self.revisionID
                                      componentsSeparatedByString:@"-"][0]] unsignedIntegerValue];
}

@end

# pragma mark CouchDocument extension


@implementation CouchDocument(RevisionTree)

// A little introduction on how CouchDB handles conflicts:
// Given the case of partitioning, two nodes might update the same document, and possibly make more than one
// change before they synchronise through replication. This results in two history lines. CouchDB will
// deterministically chose one of these lines as 'winner', and mark the revisions from the other
// line as _deleted (marking them for deletion upon compaction), while storing their revision ids in _conflicts.
//
// Given the scenario described above, the following function will analyse a document's history and determine
// the head revision of the losing line (losingHead), and the common ancestor from which the history diverged
//
// This scenario of course is a simplification as there may be an arbitrary number of such revision lines.

-(BOOL)determineLosingHead:(CouchRevision**)losingHead AndCommonAncestor:(CouchRevision**)ancestor
{
    *losingHead = nil;
    *ancestor = nil;
    NSMutableArray* losingRevs = [NSMutableArray arrayWithArray:self.getConflictingRevisions];
    
    // if getConflictingRevisions only returns one or no revsions, there's no history tree to analyse
    if ( losingRevs.count < 2 )
        return NO;
    // getConflictingRevisions also contains the current revision, but we're only interesed in the losing history line
    // here
    [losingRevs removeObject:self.currentRevision];
    
    
    // find the highest and the lowest revisions among the conflicting revs
    NSUInteger highestSeqNum = 0, lowestSeqNum = NSUIntegerMax;
    
    for ( CouchRevision* rev in losingRevs )
    {
        if ( rev.sequenceNumber == ((CouchRevision*)*losingHead).sequenceNumber )
            NSLog( @"warning: revision history has more than 2 branches" );
        if ( rev.sequenceNumber < lowestSeqNum )
            lowestSeqNum = rev.sequenceNumber;
        if ( rev.sequenceNumber > highestSeqNum )
        {
            highestSeqNum = rev.sequenceNumber;
            // the losing rev with the highest revision must be the head
            *losingHead = rev;
        }
    }
    
    for ( CouchRevision* rev in self.getRevisionHistory )
    {
        // the rev with a sequence num below the lowest seq num must be the ancestor
        if ( rev.sequenceNumber + 1 == lowestSeqNum )
            *ancestor = rev;
    }
    
    return ( [*losingHead isKindOfClass:CouchRevision.class] &&
             [*ancestor   isKindOfClass:CouchRevision.class] );
}


@end

