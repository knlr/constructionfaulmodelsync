//
//  MyCouchUITableSource.h
//  ConstructionFaultModelSync
//
//  Created by Knut Lorenzen on 22/03/2013.
//  Copyright (c) 2013 Selling Sollutions. All rights reserved.
//
// Class MyCouchUITableSource
//
// Description:
// subclass to CouchUITableSource that enables space unit relationship editing

#import <CouchCocoa/CouchUITableSource.h>
#import <CouchCocoa/CouchCocoa.h>

@interface MyCouchUITableSource : CouchUITableSource

@end
