//
//  AddRelationViewController.m
//  ConstructionFaultModelSync
//
//  Created by Knut Lorenzen on 26/03/2013.
//  Copyright (c) 2013 Selling Sollutions. All rights reserved.
//

#import "AddRelationViewController.h"
#import "ConstructionError.h"
#import "ConstructionFaultModel.h"

@interface AddRelationViewController ()
@property (nonatomic, strong) CouchLiveQuery* query;
@property (nonatomic, strong) NSArray* rows;
@property (nonatomic, strong) ConstructionFaultModel* faultModel;
@end

@implementation AddRelationViewController

-(void)viewWillAppear:(BOOL)animated
{
    
    self.faultModel = [ConstructionFaultModel default];
    
    // This view is supposed to show all units except the ones currently displayed in the units table.
    // Unfortunately, the CouchDB query APIs don't have anything like an exclude parameter.
    //
    // Therefore the query results need to be filterered. This is why I don't use CouchUITableSource here. This class
    // works pretty similar to it though (maybe I should have subclassed it rather)
    
    [self.faultModel.designDoc defineViewNamed:@"units_to_add" mapBlock:MAPBLOCK({
        if ( [((NSString*)doc[@"type"]) isEqualToString:@"unit"] )
            // since I want to filter units that are related to a specific space, I emit the related spaces
            // of each unit 
            emit( doc[@"name"], doc[@"spaces"]);
    }) version:@"1"];
    self.query = [[self.faultModel.designDoc queryViewNamed:@"units_to_add"] asLiveQuery];
    
    // a CouchLiveQuery is a persistent query object that gets notified by KVO everytime its results change    
    [self.query addObserver:self forKeyPath:@"rows" options:0 context:NULL];
    [self.query start];
    self.rows = [NSArray array];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.query removeObserver:self forKeyPath:@"rows"];    
}


-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ( object == self.query )
    {
        NSString *selectedSpaceId = [ConstructionError default].space;
        NSAssert( selectedSpaceId.length > 0, @"" );
        NSMutableArray* newRows = [NSMutableArray array];
        // filter out any units that have a reference to the selected space
        for ( CouchQueryRow* row in self.query.rows )
        {
            NSArray* spaces = row.value;
            if ( ![spaces containsObject:selectedSpaceId] )
                [newRows addObject:row];
        }
        self.rows = newRows;
        [self.tableView reloadData];
    }
}


   
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* reuseIdentifier = @"AddRelation";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if ( !cell )
        cell =[[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault
                             reuseIdentifier:reuseIdentifier];

    cell.textLabel.text = ((CouchQueryRow*)self.rows[indexPath.row]).key;
    return cell;    
}
 



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.rows count];
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // dismiss the popover and add a reference to the selected space to the selected unit
    [[self popoverCtrlr] dismissPopoverAnimated:YES];
    NSString* spaceIdToAdd = [ConstructionError default].space;
    CouchDocument *selectedUnit = ((CouchQueryRow*)self.rows[indexPath.row]).document;
    NSMutableArray* modSpaceRefs = [NSMutableArray arrayWithArray:selectedUnit[@"spaces"]];
    NSAssert( ![modSpaceRefs containsObject:spaceIdToAdd], @"" );
    [modSpaceRefs addObject:spaceIdToAdd];
    NSMutableDictionary *modProps = [NSMutableDictionary dictionaryWithDictionary:selectedUnit.properties];
    modProps[@"spaces"] = modSpaceRefs;
    
    [[selectedUnit putProperties:modProps] onCompletion:^(){}];
}


@end
