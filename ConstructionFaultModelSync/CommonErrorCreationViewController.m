//
//  CommonErrorCreationViewController.m
//  ConstructionFaultModelSync
//
//  Created by Knut Lorenzen on 10/03/2013.
//  Copyright (c) 2013 Selling Sollutions. All rights reserved.
//

#pragma mark

#import "CommonErrorCreationViewController.h"
#import <CouchCocoa/CouchCocoa.h>
#import "ConstructionFaultModel.h"


#pragma mark CommonErrorCreationViewController()

@interface CommonErrorCreationViewController ()

@end


#pragma mark CommonErrorCreationViewController implementation

@implementation CommonErrorCreationViewController

#pragma mark class variables


static  UISwitch* __connectionSwitch = nil;

#pragma mark view lifecycle



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.currentError = [ConstructionError default];
    self.faultModel = [ConstructionFaultModel default];    
    
    if ( __connectionSwitch == nil )
    {
        __connectionSwitch = [UISwitch new];
        [__connectionSwitch addTarget:self action:@selector(didToggleConnectionSwitch:)
                     forControlEvents:UIControlEventValueChanged];
        
        [__connectionSwitch setOn:NO animated:NO];
        // to launch without replication, initialise the UISwitch with setOn:NO
        
        [self didToggleConnectionSwitch:__connectionSwitch];
    }
    
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:__connectionSwitch];
    
    self.navigationItem.leftItemsSupplementBackButton = YES;
    self.navigationItem.leftBarButtonItems =  @[self.editButtonItem, item];
    
   
    
    // init add relation popover
    self.addRelationVC = [[AddRelationViewController alloc]
                     initWithNibName:@"AddRelationViewController"
                     bundle:nil];
    self.addRelationPopoverC = [[UIPopoverController alloc] initWithContentViewController:self.addRelationVC];
    self.addRelationVC.popoverCtrlr = self.addRelationPopoverC;
    [self.addRelationPopoverC setPopoverContentSize:CGSizeMake(320, 800)];
}

#pragma mark replication

-(void)didToggleConnectionSwitch:(UISwitch*)uiSwitch
{
    // this is the whole synchronisation logic:
    NSURL* remoteUrl;
    if ( uiSwitch.isOn )
        remoteUrl = [NSURL URLWithString:@"http://127.0.0.1:5984/defect_model"];
    else
        // an empty NSURL will stop the replication
        remoteUrl = [NSURL new];
    
    NSArray* r = [self.faultModel.designDoc.database replicateWithURL:remoteUrl exclusively:YES];
    
    // just to demo the filtering; here's a server side-filter (a dummy function should exist inside
    // _design/default/filters/test)
    CouchPersistentReplication *pull = r[0];
    pull.filter = @"default/test";
    
    // and a client side filter that is run by the push replication:
    [self.faultModel.designDoc defineFilterNamed:@"test" block:^BOOL(TD_Revision *revision,
                                                           NSDictionary *params) {
        return YES;
    }];
    CouchPersistentReplication *push = r[1];
    push.filter = @"iOS/test";
    
    // - replicateWithURL starts continous two-way synchronisation
    //
    // - all changes on both side are instantly being replicated as long as a network connection is available.
    //   Previous changes that were missed during an interruption are of course also being replayed
    //
    // - if a document was modified by both sides before it could be replicated, CouchDB and TouchDB deterministically
    //   pick a winning revision and mark the losing one as _deleted (more about that in ConstructionFaultModel)
    //
    // - it is possible to apply filters to both directions by defining a filter function in a design document
    //   and setting its name as a property of the CouchReplication object,
    //   e.g. myCouchReplication.filter = @"my_design/my_filter"
    //
    // - a filter function is somewhat similar to a map funtion, the difference being that it retuns true or false
    //   instead of calling 'emit' (just like a validation function)
    //
    // - pull filters are executed and stored on the CouchDB server and therefore written in Javascript, while
    //   push filters need to be Objective-C blocks (see CouchDesignDocument::defineFilterNamed:block:)
  
}

#pragma mark handle selections & deselections

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if      ( tableView == self.leftTableView )
        [self didSelectLeftTableViewAtIndex:indexPath.row];
    else if ( tableView == self.centerTableView )
        [self didSelectCenterTableViewAtIndex:indexPath.row];
    else if ( tableView == self.rightTableView )
        [self didSelectRightTableViewAtIndex:indexPath.row];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( ![tableView isEditing] )
    {
        // if an already selected cell is selected again, deselect it
        if ( [indexPath compare:[tableView indexPathForSelectedRow]] == NSOrderedSame ) {
            
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            if      ( tableView == self.leftTableView )
                [self didDeselectLeftTableView];
            else if ( tableView == self.centerTableView )
                [self didDeselectCenterTableView];
            else if ( tableView == self.rightTableView )
                [self didDeselectRightTableView];
            return nil;
        }
        else {
            return indexPath;
        }
    }
    else
    {
        // in edit mode: only allow top row to be selected
        if ( [indexPath row] == 0 )
            return indexPath;
        else
            return nil;
    }
}

#pragma mark conflict management







#pragma mark methods stubs to be overwritten

-(void)didSelectLeftTableViewAtIndex:(NSInteger)index {}
-(void)didSelectCenterTableViewAtIndex:(NSInteger)index {}
-(void)didSelectRightTableViewAtIndex:(NSInteger)index {}
-(void)didDeselectLeftTableView {}
-(void)didDeselectCenterTableView {}
-(void)didDeselectRightTableView {}

@end
