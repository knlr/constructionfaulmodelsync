//
//  ConstructionFaultModel.h
//  ConstructionFaultModelSync
//
//  Created by Knut Lorenzen on 22/04/2013.
//  Copyright (c) 2013 Selling Sollutions. All rights reserved.
//
// Class ConstructionFaulModel
//
// Description:
// Holds logic that is specific to the construction fault database and not directly related to the UI,
// e.g. views, queries, conflict handling etc.

#import <Foundation/Foundation.h>
#import <CouchCocoa/CouchCocoa.h>

@interface ConstructionFaultModel : NSObject

+(ConstructionFaultModel*)default;

@property (readonly) CouchDesignDocument* designDoc;

-(CouchLiveQuery*)querySpaces;
-(CouchLiveQuery*)queryUnits;
-(CouchLiveQuery*)queryObjects;
-(CouchLiveQuery*)queryDetails;
-(CouchLiveQuery*)queryErrorCodes;
-(CouchLiveQuery*)queryLocations;


@end
