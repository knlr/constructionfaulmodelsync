//
//  ErrorCreationViewController.m
//  ConstructionFaultModelSync
//
//  Created by Knut Lorenzen on 14/02/2013.
//  Copyright (c) 2013 Selling Sollutions. All rights reserved.
//

#import "Step1ViewController.h"
#import "Step2ViewController.h"
#import "ConstructionError.h"

@interface Step1ViewController ()

@end


@implementation Step1ViewController 

#pragma mark lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    self.spacesTableSource.query = self.faultModel.querySpaces;
    for ( CouchUITableSource *s in @[self.spacesTableSource,
                                     self.unitsTableSource,
                                     self.objectsTableSource] ) {
        s.deletionAllowed = NO;
    }
  
    UIBarButtonItem* fixedSpace = [[UIBarButtonItem alloc]
                                   initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                        target:nil
                                                        action:0];
    fixedSpace.width = 58;
    self.navigationItem.leftBarButtonItems = [@[fixedSpace]
                                              arrayByAddingObjectsFromArray:
                                              self.navigationItem.leftBarButtonItems];
}

# pragma mark select


-(void)didSelectLeftTableViewAtIndex:(NSInteger)index
{
    CouchQueryRow* row = self.spacesTableSource.rows[index];
    NSString *selectedSpace = row.documentID;
    self.currentError.space = selectedSpace;
        
    // center: show units belonging to the selected space  
    self.unitsTableSource.query = self.faultModel.queryUnits;
    [self.unitsTableSource reloadFromQuery];
    
    // right hand side: related objects    
//    self.objectsTableSource.query = [self.faultModel objectsForSpace:selectedSpace];
    self.objectsTableSource.query = self.faultModel.queryObjects;
    [self.objectsTableSource reloadFromQuery];   
}


-(void)didSelectCenterTableViewAtIndex:(NSInteger)index
{
    BOOL inEditMode = [[self centerTableView] isEditing] && index == 0;
    
    if ( inEditMode )
    {
        CGRect rect = [self.leftTableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:(NSUInteger)index
                                                                                   inSection:0]];       
        
        [[self addRelationPopoverC] presentPopoverFromRect:rect
                                                    inView:[self centerTableView]
                                  permittedArrowDirections:UIPopoverArrowDirectionAny
                                                  animated:YES];
    }
    else {
        CouchQueryRow* row = self.unitsTableSource.rows[index];
        self.currentError.unit = row.documentID;
        
        // right hand side: show objects belonging to the selected unit (if any)                
        self.objectsTableSource.query = self.faultModel.queryObjects;
        [self.objectsTableSource reloadFromQuery];
    }
}




-(void)didSelectRightTableViewAtIndex:(NSInteger)index
{
    CouchQueryRow* row = self.objectsTableSource.rows[index];
    self.currentError.object = row.documentID;
}



# pragma mark de-select

-(void)didDeselectLeftTableView
{
    CouchQuery* all_docs = [self.faultModel.designDoc.database getAllDocuments];
    [all_docs setKeys:@[]];
    
    self.unitsTableSource.query = [all_docs asLiveQuery];
    [self.unitsTableSource reloadFromQuery];
    
    self.objectsTableSource.query = [all_docs asLiveQuery];
    [self.objectsTableSource reloadFromQuery];
    
    self.currentError.space = nil;
    self.currentError.unit = nil;
    self.currentError.object = nil;
}




-(void)didDeselectCenterTableView
{
    self.currentError.unit = nil;
    self.currentError.object = nil; 
    
    self.objectsTableSource.query = self.faultModel.queryObjects;
    [self.objectsTableSource reloadFromQuery];
}



-(void)didDeselectRightTableView
{
    self.currentError.object = nil;
}

-(void)clearAllSelections
{    
    [self.leftTableView deselectRowAtIndexPath:self.leftTableView.indexPathForSelectedRow animated:YES];
    [self didDeselectLeftTableView];    
    [self.leftTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                              atScrollPosition:UITableViewScrollPositionTop
                                      animated:YES];
}


#pragma mark view transition 

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    Step2ViewController* step2vc = [segue destinationViewController];
    NSAssert( [step2vc isKindOfClass:[Step2ViewController class]],
             @"%s %d", __FUNCTION__, __LINE__ );
    step2vc.step1ViewController = self;
}

#pragma mark editing mode

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( ![tableView isEditing] )
        return UITableViewCellEditingStyleNone;
    else
    {
        if ( [indexPath row] == 0 )
            return UITableViewCellEditingStyleInsert;
        else
            return UITableViewCellEditingStyleDelete;
    }
}


- (void)setEditing:(BOOL)editing animated:(BOOL)animate
{
    // only allow unit editing while a space is selected
    if ( [[self currentError] space] )
    {
        [super setEditing:editing animated:animate];
        [[self unitsTableSource] setDeletionAllowed:editing];

        // insert/delete a special cell to add new items
        [[self centerTableView] beginUpdates];
        NSArray *insertIndexPath = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]];
        if ( editing ) {
            [[self centerTableView] insertRowsAtIndexPaths:insertIndexPath withRowAnimation:UITableViewRowAnimationTop];
        }
        else {
            [[self centerTableView] deleteRowsAtIndexPaths:insertIndexPath withRowAnimation:UITableViewRowAnimationTop];
        }
        [[self centerTableView] setEditing:editing animated:animate];
        [[self centerTableView] endUpdates];
    }

}

@end

