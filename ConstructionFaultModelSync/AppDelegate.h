//
//  AppDelegate.h
//  ConstructionFaultModelSync
//
//  Created by Knut Lorenzen on 14/02/2013.
//  Copyright (c) 2013 Selling Sollutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CouchCocoa/CouchCocoa.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

