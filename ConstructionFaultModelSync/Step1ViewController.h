//
//  Step1ViewController.h
//  ConstructionFaultModelSync
//
//  Created by Knut Lorenzen on 14/02/2013.
//  Copyright (c) 2013 Selling Sollutions. All rights reserved.
//
// Class Step1ViewController
//
// Description:
// Parent view controller for the space, units and objects tables



#import "CommonErrorCreationViewController.h"

@interface Step1ViewController : CommonErrorCreationViewController

-(void)clearAllSelections;

@property (weak, nonatomic) IBOutlet MyCouchUITableSource *unitsTableSource;
@property (weak, nonatomic) IBOutlet CouchUITableSource *objectsTableSource;
@property (weak, nonatomic) IBOutlet CouchUITableSource *spacesTableSource;

@end
