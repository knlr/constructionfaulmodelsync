//
//  CommonErrorCreationViewController.h
//  ConstructionFaultModelSync
//
//  Created by Knut Lorenzen on 10/03/2013.
//  Copyright (c) 2013 Selling Sollutions. All rights reserved.
//
// Class CommonErrorCreationViewController
//
// Description:
// Abstract subclass to the screen view controllers.
// Holds shared objects, routes table events into method stubs

#import <UIKit/UIKit.h>
#import <CouchCocoa/CouchCocoa.h>
#import <CouchCocoa/CouchUITableSource.h>
#import "ConstructionError.h"
#import "MyCouchUITableSource.h"
#import "AddRelationViewController.h"
#import "ConstructionFaultModel.h"

@class  CouchDatabase;
@class ConstructionError;


@interface CommonErrorCreationViewController : UIViewController
<CouchUITableDelegate, UIPopoverControllerDelegate>


@property (weak, nonatomic) ConstructionError* currentError;
@property (weak, nonatomic) ConstructionFaultModel* faultModel;

@property (weak, nonatomic) IBOutlet UITableView *leftTableView;
@property (weak, nonatomic) IBOutlet UITableView *centerTableView;
@property (weak, nonatomic) IBOutlet UITableView *rightTableView;

@property (strong, nonatomic) AddRelationViewController *addRelationVC;
@property (strong, nonatomic) UIPopoverController *addRelationPopoverC;

-(void)didSelectLeftTableViewAtIndex:(NSInteger)index;
-(void)didSelectCenterTableViewAtIndex:(NSInteger)index;
-(void)didSelectRightTableViewAtIndex:(NSInteger)index;

-(void)didDeselectLeftTableView;
-(void)didDeselectCenterTableView;
-(void)didDeselectRightTableView;


@end