//
//  ConstructionError.h
//  ConstructionFaultModelSync
//
//  Created by Knut Lorenzen on 12/03/2013.
//  Copyright (c) 2013 Selling Sollutions. All rights reserved.
//
// Class ConstructionError
//
// Description:
// Represents the current construction error being created (singleton)
//

#import <Foundation/Foundation.h>

@class CouchDatabase;

@interface ConstructionError : NSObject

+(ConstructionError*)default;

@property (strong, nonatomic) NSString *space;
@property (strong, nonatomic) NSString *unit;
@property (strong, nonatomic) NSString *object;
@property (strong, nonatomic) NSString *detail;
@property (strong, nonatomic) NSString *errorCode;
@property (strong, nonatomic) NSString *location;

-(void)saveToDatabase:(CouchDatabase*)database;

@end