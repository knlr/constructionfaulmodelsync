//
//  Step2ViewController.m
//  ConstructionFaultModelSync
//
//  Created by Knut Lorenzen on 09/03/2013.
//  Copyright (c) 2013 Selling Sollutions. All rights reserved.
//



#import "Step1ViewController.h"
#import "Step2ViewController.h"


@interface Step2ViewController ()

@end


@implementation Step2ViewController


#pragma mark view initialisation

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSAssert( [self.step1ViewController isKindOfClass:[Step1ViewController class]] ,
             @"%s %d", __FUNCTION__, __LINE__ );

    for ( CouchUITableSource *s in @[
         self.detailsTableSource,
         self.errorCodesTableSource,
         self.locationsTableSource] ) {
        s.deletionAllowed = NO;        
    }
    
    self.detailsTableSource.query = self.faultModel.queryDetails;
    self.errorCodesTableSource.query = self.faultModel.queryErrorCodes;
    
    [self.detailsTableSource reloadFromQuery];
    [self.errorCodesTableSource reloadFromQuery];
    
   
    self.locationsTableSource.query = self.faultModel.queryLocations;
    self.rightTableView.dataSource = self.locationsTableSource;
    
}

#pragma mark selection handling


-(void)didSelectLeftTableViewAtIndex:(NSInteger)index
{
    CouchQueryRow* row = self.detailsTableSource.rows[index];
    self.currentError.detail = row.documentID;    
    
    self.errorCodesTableSource.query = self.faultModel.queryErrorCodes;    
    [self.errorCodesTableSource reloadFromQuery];
    
}



-(void)didSelectCenterTableViewAtIndex:(NSInteger)index
{
    CouchQueryRow* row = self.errorCodesTableSource.rows[index];
    self.currentError.errorCode = row.documentID;
}



-(void)didSelectRightTableViewAtIndex:(NSInteger)index
{
    CouchQueryRow* row = self.locationsTableSource.rows[index];
    self.currentError.location = row.documentID;
}



-(void)didDeselectLeftTableView
{
    self.currentError.detail = nil;
    CouchQuery* all_docs = [self.faultModel.designDoc.database getAllDocuments];
    [all_docs setKeys:@[]];
    [[self errorCodesTableSource] setQuery:[all_docs asLiveQuery]];
    [[self errorCodesTableSource] reloadFromQuery];
}



-(void)didDeselectCenterTableView
{
    self.currentError.errorCode = nil;
}



-(void)didDeselectRightTableView
{
    self.currentError.location = nil;
}


#pragma mark view transition




- (IBAction)didPressSave:(UIBarButtonItem *)sender
{
    [self.currentError saveToDatabase:self.faultModel.designDoc.database];
    
    self.currentError.detail = nil;
    self.currentError.errorCode = nil;
    self.currentError.location = nil;
    
    [self.step1ViewController clearAllSelections];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
