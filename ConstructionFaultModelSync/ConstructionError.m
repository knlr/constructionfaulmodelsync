//
//  ConstructionError.m
//  ConstructionFaultModelSync
//
//  Created by Knut Lorenzen on 12/03/2013.
//  Copyright (c) 2013 Selling Sollutions. All rights reserved.
//

#import "ConstructionError.h"
#import <CouchCocoa/CouchCocoa.h>

@implementation ConstructionError

static ConstructionError *__defaultInstance = nil;

+(ConstructionError*)default
{
    if ( !__defaultInstance )
        __defaultInstance = [ConstructionError new];
    return __defaultInstance;
}

-(void)saveToDatabase:(CouchDatabase*)database
{
    NSMutableDictionary *docProps = [NSMutableDictionary dictionary];
    
    docProps[@"type"] = @"construction_fault";
    docProps[@"created_at"] = [RESTBody JSONObjectWithDate: [NSDate date]];
    if ( self.space )
         docProps[@"space"] = self.space;
    if ( self.unit )
        docProps[@"unit"]   = self.unit;
    if ( self.object )
        docProps[@"object"] = self.object;
    if ( self.detail )    
        docProps[@"detail"] = self.detail;
    if ( self.errorCode )
        docProps[@"error_code"] =  self.errorCode;
    if ( self.location )
        docProps[@"location"] =   self.location;


    CouchDocument *newDoc = [database untitledDocument];
    RESTOperation* op = [newDoc putProperties:docProps];
    [op onCompletion: ^{
        if (op.error)
            NSLog(@"failed to insert construction error into local database %@", op.responseBody.asString );
    }];
    [op start];
}

@end
