//
//  Step2ViewController.h
//  ConstructionFaultModelSync
//
//  Created by Knut Lorenzen on 09/03/2013.
//  Copyright (c) 2013 Selling Sollutions. All rights reserved.
//
// Class Step2ViewController
//
// Description:
// Parent view controller for the details, error codes and location tables
//

#import "CommonErrorCreationViewController.h"

@class Step1ViewController;

@interface Step2ViewController : CommonErrorCreationViewController
- (IBAction)didPressSave:(UIBarButtonItem *)sender;

@property (weak, nonatomic) IBOutlet CouchUITableSource *detailsTableSource;
@property (weak, nonatomic) IBOutlet CouchUITableSource *errorCodesTableSource;
@property (weak, nonatomic) IBOutlet CouchUITableSource *locationsTableSource;

@property(weak, nonatomic) Step1ViewController* step1ViewController;

@end
