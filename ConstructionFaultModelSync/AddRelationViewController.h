//
//  AddRelationViewController.h
//  ConstructionFaultModelSync
//
//  Created by Knut Lorenzen on 26/03/2013.
//  Copyright (c) 2013 Selling Sollutions. All rights reserved.
//
// Class AddRelationViewController
//
// Description:
// Shows a table view with all units which are not related to the currently selected space
// and adds a relation when a row is selected


#import <UIKit/UIKit.h>
#import <CouchCocoa/CouchCocoa.h>


@interface AddRelationViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;



@property (weak, nonatomic) UIPopoverController* popoverCtrlr;



@end
