//
//  MyCouchUITableSource.m
//  ConstructionFaultModelSync
//
//  Created by Knut Lorenzen on 22/03/2013.
//  Copyright (c) 2013 Selling Sollutions. All rights reserved.
//

#import "MyCouchUITableSource.h"
#import <CouchCocoa/CouchCocoa.h>
#import "ConstructionError.h"

@implementation MyCouchUITableSource


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
                                            forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( editingStyle == UITableViewCellEditingStyleDelete )
    {        
        NSString* selectedSpaceId = [ConstructionError default].space;
        NSAssert( selectedSpaceId.length > 0 , @"");
        
        // in edit mode the indexPath has an offset of 1
        NSIndexPath* offsetIndexPath = [NSIndexPath indexPathForRow:indexPath.row-1
                                                          inSection:indexPath.section];
        CouchDocument* selectedUnit = [self documentAtIndexPath:offsetIndexPath];
        NSAssert( [selectedUnit isKindOfClass:CouchDocument.class], @"" );        
        NSAssert( [((NSString*)selectedUnit[@"type"]) isEqualToString:@"unit"], @"" );
        
        // remove the reference to the selected space from the selected unit's document
        NSMutableArray* docSpaces = [NSMutableArray arrayWithArray:selectedUnit[@"spaces"]];
        NSAssert( [docSpaces containsObject:selectedSpaceId], @"" );
        [docSpaces removeObject:selectedSpaceId];
        
        NSMutableDictionary* selectedUnitProps = [NSMutableDictionary dictionaryWithDictionary:selectedUnit.properties];
        selectedUnitProps[@"spaces"] = docSpaces;
        [[selectedUnit putProperties:selectedUnitProps] onCompletion:^(){ }];
        // putProperties creates a RESToperation, which needs to be referenced until it's finished. This is done here
        // by assigning it an empty completion block
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( ![tableView isEditing]  )
    {
        return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }
    else
    {
        // in editing mode
        if ( [indexPath row] == 0 )
        {
            // return a special cell for the top row
            NSString* insertionCellReuseId = @"CouchUITableDelegateInsert";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:insertionCellReuseId];
            if (!cell)
                cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault
                                              reuseIdentifier:insertionCellReuseId];
            [[cell textLabel] setText:@"add ..."];
            return cell;
            
        }
        else {
            // and move the existing cells one row downwards
            NSIndexPath* offsetIndexPath = [NSIndexPath indexPathForRow:[indexPath row]-1 inSection:[indexPath section]];
            return [super tableView:tableView cellForRowAtIndexPath:offsetIndexPath];
        }
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowsCount = [super tableView:tableView numberOfRowsInSection:section];
    if ( ![tableView isEditing] )
        return rowsCount;
    else
        return rowsCount + 1;
 
}

@end
