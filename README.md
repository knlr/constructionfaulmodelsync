ConstructionFaultModelSync
==========================

A project that attempts to sync a TouchDB database with a hosted CouchDB database
## From Tables to Documents

Even though CouchDB allows arbitrarily structured documents, it is still necessary to normalize the data in most cases (http://guide.couchdb.org/draft/design.html#modeling). This is usually done by defining a document type per entity i.e. attaching a property called 'type' containing a string describing its type to each document.
For instance, for each row in the table "DM_Spaces" a document is created, containing a property for each table column and "space" for its type. This done for the other entities respectively.


Documents must have a unique id. There are no constraints whatsoever and any string may be used. If at creation time no id property is given, CouchDB will automatically generate one ([which is discouraged however](http://wiki.apache.org/couchdb/FUQ#Documents)). I have chosen the id to be a concatenation of the type and the name to be displayed. This ensures that this combination is unique (http://guide.couchdb.org/draft/cookbook.html#uniqueness) and in some cases facilitates alphabetical sorting. It may be a bad choice if entities are renamed frequently, as renaming means also changing the document id and hence replacing the document, breaking any references to it.



## Representing SQL queries as CouchDB views

Documents may contain references to other documents in form of document ids. There are however no referential constraints in CouchDB and it's up to the application to keep these references consistent.

A many-to-many relationship may be modeled by either entity holding a list of references to the other one ([Relationship documents](http://wiki.apache.org/couchdb/EntityRelationship#Many_to_Many:_Relationship_documents) that hold the ids of two related documents are also an option).


If we look at the relation between spaces and units as an example, we have a choice to make: Should the spaces store the ids of the units they are related to, or the other way around?

### spaces -> units
Considering we go with the first approach (which was implemented up until commit c4453a7d00f9fd485cdf727b8ce5f3efab7a0d14), a document representing a space would look like this:

    { 
        _id : "space x",
        type : "space",
        name : "name space x",
        units : [ "unit a", "unit c", "unit e"]
    }

To get the units related to a given space, we first create a view from the following map function:

    function(doc){
        if ( doc.type == "unit")
            emit( doc.type, doc.name );
    }

Map filters out all documents that are not units, and emits for every unit its id as key with its name as value. The view generated from this will have rows structured like this:

    key: "unit a" value: "name unit a"
    key: "unit b" value: "name unit b"
    key: "unit c" value: "name unit c"
    key: "unit d" value: "name unit d"
    key: "unit e" value: "name unit e" 

This is a simple index to look up a unit's name by its document id. If a space is selected, we need to extract the ids of the related units out of the space document, and use them to specify a list of keys to the query: 

    keys=["unit a", "unit c", "unit e"]

Query result:

    key: "unit a" value: "name unit a"
    key: "unit c" value: "name unit c" 
    key: "unit e" value: "name unit e"


### spaces <- units
So how would we achieve the same query result if the relation were modeled the other way around? This is what a unit would look like:

    { 
        _id : "unit a",
        type : "unit",
        name: "name of unit a"
        spaces : [ "space x", "space y", "space z" ]
    }

To query for units for a given space, we can generate a view like this:

    function(doc) {
        if ( doc.type == "unit" )
            for each ( space in doc.spaces )
                emit( space, doc.name );
    }

The map function iterates over the spaces array of every unit document, producing a view that looks something like this:

    key: "space x" value: "name unit a"
    key: "space x" value: "name unit b" 
    key: "space x" value: "name unit c" 
    key: "space y" value: "name unit a" 
    key: "space y" value: "name unit c" 
    key: "space z" value: "name unit a" 

Notice how the view is sorted after the space id and contains multiple entries with the same key (the order in which emit was called is irrelevant). If we query it with the id of a given space as key (e.g. key="space x"), it will constrain the results as follows:

    key: "space x" value: "name unit a"
    key: "space x" value: "name unit b" 
    key: "space x" value: "name unit c" 


The advantages of this approach are that the application doesn't need to fetch the full space document and extract a (potentially long) list of document ids, and the query performance should be better because CouchDB only has to look up a single key. This makes more use of CouchDB views. 
Since in practice we are most likely interested in units for a given space, storing references to spaces inside the unit documents seems to be a better choice.

See also: http://www.cmlenz.net/archives/2007/10/couchdb-joins

### Transitive relations

The new version of the defect model introduces what I refer to as transitive relations, like for example `spaces->units->objects`. In SQL these are represented as follows:

    "SELECT DISTINCT DM_Object.dm_ObjectID 
    FROM DM_Object 
    INNER JOIN DM_DefectRelation 
    ON DM_Object.dm_ObjectID = DM_DefectRelation.dm_ObjectID 
    WHERE 
    DM_DefectRelation.dm_SpaceID = %d AND
    DM_DefectRelation.dm_UnitID = %d AND 
    DM_DefectRelation.dm_ObjectID IS NOT NULL",
    spaceId,unitId;    

The relations table defines relations between spaces, units and objects. These are independent from relations that involve only spaces and objects or spaces and units. For a given combination of a space and a unit, one or more objects are related to that pair. The application is interested in objects belonging to a (space, unit) pair, because some objects make only sense in the context of a specific unit inside a specific space.

When we translate this to CouchDB views, it would be desirable to have a view with such space-unit pairs as keys and objects as values:

    key: ["space a", "unit b"]  value: "name of object c"

Applying the method we used above to store many-to-many relations, we could simply attach an array of these (space, unit) pairs to the object documents:

    {
        _id  : "object x"
        type : "object",
        name : "name of object"     
        spaces_units : [["space a", "unit a"],
                        ["space a", "unit b"],
                        ["space b," "unit c"],
                        ...],
    }

To create the desired view, the map function then would simply iterate over that array:

    function(doc) {
        if ( doc.type == "object" )
            for each ( space_unit in doc.spaces_units )
                emit( space_unit, doc.name );
    }

However storing the relations like that is a bit redundant since the space id is repeated for every unit. Instead of (space, unit) pairs, one might as well build a nested object, which is more compact:

    {
        type : "object",
        name : "name of object"
        spaces_units : {
            "space a" : ["unit a", "unit b"],
            "space b" : ["unit c", ...],
            ...
        }
    }

And then use a map function that has nested loops, one iterating over the space ids and one over the unit ids:

    function(doc){
        if ( doc.type == "object" )
            for ( space in doc.spaces )
                for each ( unit in doc.spaces[space] )
                    emit( [space, unit], doc.name )
    }

Which will generate the view described above:

    key: ["space a", "unit a"]  value: "name of object x"
    key: ["space a", "unit a"]  value: "name of object y"   
    key: ["space a", "unit b"]  value: "name of object x"
    key: ["space b", "unit c"]  value: "name of object z"
    ...

And allow to query for objects related to a space, unit pair by using their ids as a compound key, i.e. key=["space a", "unit a"]


## Traversing the path of views

The mechanisms described in the two previous sections are applied for all occurring relations:

- space->unit
- space->object
- space->unit->object
- space->unit->detail
- space->object->detail
- space->unit->object->detail
- space->error code
- space->unit->error code
- space->object->error code
- space->unit->object->error code
- space->unit->detail->error code
- space->object->detail->error code
- space->unit->object->detail->error code

For each of the original SQL-Queries, a corresponding property containing transitive relation paths (or flat relations) is added to the document representing the last element of the path. For instance, a document representing a object might have the properties `spaces` and `spaces_units`. And for each of the above relations, a CouchDB view is created. 

## About grouping sorting compound keys

Views are sorted after their keys when they are generated, and the ordering cannot be changed at query time (except for reversing through 'descending=true').

Keys in CouchDB views are not restricted to strings or numbers, they can be structured as well. A common practice is to use arrays of strings, so-called compound keys, to influence how the views are sorted. How are compound keys sorted? CouchDB compares the first element, then the second, and so on. If two keys are identical in the first element, but one has more elements than the other, the shorter key has a higher sorting priority. 

    key: ["aaa", ""]        value: …
    key: ["aaa", "bbb"]     value: …
    key: ["bbb", "aaa"]     value: …
    key: ["bbb", "bbb"]     value: …
    key: ["ccc" ]           value: …
    key: ["ccc", "aaa"]     value: …    

Let's pretend we're interested in all rows whose key has "bbb" in the first element. We can't use the 'key' or 'keys' parameter, since we don't know the exact keys we're looking for. Instead we can specify a range of keys:

    startkey = ["bbb"]
    endkey   = ["bbb", {} ]

["bbb"] has the highest sorting priority of any array that starts with "bbb". And an empty object has a lower sorting priority than any string. So this range will include any key that is an array with the string "bbb" as its first element.

See also: http://wiki.apache.org/couchdb/View_collation


## How to resolve conflicts

### Multi version concurrency control (MVCC)
Documents have a revision history, and each time they are updated, a new 
revision is created and the previous one is marked as deleted. Documents have a reserved property \_rev that holds the revision id.  They look like this: 1-30309a27fb031de5b29f778a151e9e8b: A sequence number reflecting the count of changes the document has gone through and a hash.

Querying a document will implicitly return the most recent revision, older revisions can be retrieved by specifying their revision id.

### Conflicting revisions

If CouchDB (typically after replication) finds that a document has conflicting revisions (or even whole history lines) it will deterministically pick a _winning revision_. The _losing revisions_ and their ancestors will NOT get marked deleted, and their revision ids will be stored in the winning revision under \_conflicts. This way, CouchDB is not interupted by conflicts while no changes are lost.

### Resolution
To mark a conflict as resolved to CouchDB, all conflicting revisions except the preferred one must be deleted. In CouchDB's RESTful API this is either done through a [DELETE request](http://wiki.apache.org/couchdb/HTTP_Document_API#DELETE) and appending the revision id as parameter, or setting the property `_deleted : true` when updating. CouchCocoa has a more abstract methods, for instance the -resolveConflictingRevisions:withRevision: or -resolveConflictingRevisions:withProperties: in the [CouchDocument class](http://labs.couchbase.com/CouchCocoa/docs/interfaceCouchDocument.html).

The fact that the losing revisions are preserved allows to compare them to the current one, and either present the user with a choice or implement automatic merging algorithms if applicable.

In the iOS client, there is some experimental code (in ConstructionFaultModel class) that resolves conflicts in the property 'spaces' inside unit documents, which is a array of strings and therefore easy merge. Steps to demo:

1. Turn off syncing in the iOS app (set the UISwitch in the navigation bar to 'off')
* Select a space, and tap 'edit'
* Add or delete a unit from that space
* In the CouchApp, select a different space, and add (or delete) the same unit
* Turn on syncing in the iOS app

Result: Both changes should be merged.

[Chapter about conflicts in The Definitive Guide](http://guide.couchdb.org/draft/conflicts.html) 


## References

1. http://guide.couchdb.org/draft/design.html#modeling
* http://guide.couchdb.org/draft/cookbook.html#uniqueness  
* http://wiki.apache.org/couchdb/EntityRelationship#Many_to_Many:_Relationship_documents
* http://www.cmlenz.net/archives/2007/10/couchdb-joins
* http://wiki.apache.org/couchdb/View_collation
* http://wiki.apache.org/couchdb/HTTP_Document_API#DELETE
* http://labs.couchbase.com/CouchCocoa/docs/interfaceCouchDocument.html
* http://guide.couchdb.org/draft/conflicts.html

More:
* CouchDB: The Definitive Guide, O'Reilly http://guide.couchdb.org
  * Finding Your Data with Views http://guide.couchdb.org/draft/views.html
  * View Cookbook for SQL Jockeys  http://guide.couchdb.org/draft/cookbook.html
* HTTP View API Reference http://wiki.apache.org/couchdb/HTTP_view_API

