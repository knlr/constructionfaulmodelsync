#!/usr/bin/ruby
# encoding: UTF-8
# Migrator.rb
#
=begin
 
This script will read a sqlite db file and create a CouchDB database, including some views to demo
the relations. The location of the databases are hard wired inside the initialize function.

For optimal performance, the documents are constructed step by step as ruby Hashes and commited in the very end 
to CouchDB. I avoid running SQL queries inside loops or doing complex joins, but rather build temporary
lookup tables between sql keys and couch doc ids.
 
The basic scheme is to map each entity table to a doc.type. 
 
The doc._id is chosen as a concatenation of the type and the name to be displayed. This ensures that this 
combination is unique and in some cases facilitates alphabetical sorting.
 It may be a bad choice if entities are renamed frequently, as renaming means changing the doc._id and hence
replacing the document, breaking any references to it. But it will prevent duplicate names.
 
The relationship modeling is driven by the way the entities are expected to be queried. For instance, units
contain a property called 'spaces' containing the doc._ids of the related spaces. It could be done the
other way around, i.e. spaces having a property called 'units', but this would make it way more complicated to
look for units for a given space. To learn how this works, check out the view 'units_by_space' in futon (for
more infor read http://wiki.apache.org/couchdb/EntityRelationship ).
 
The new chained relationships made it necessary to add several, more complex such reference properties to some
doc.types. For example objects have one property called 'spaces', and one called 'spaces_units'. The spaces
stored inside 'doc.spaces' determine wether the object is shown when ONLY a space is selected. 
 
The doc.spaces_units property on the other hand determines to which combinations of space and unit a given object
belongs. At the top level it contains a property for each space, of which each contains an array with units. To
understand how this works it is recommended to examine the view named 'objects_by_space_unit' in futon. Other, 
more complex relationships use the same principle, they just have more layers of nesting.

=end

require 'rubygems'
require 'sqlite3'
require 'couchrest'

class Migrator
    
    attr_reader :couch, :sql, :docs, :doc_id_by_type_and_sql_id
    @@Design = 'default'
    @@Space = 'space'
    @@ErrorCode = 'error_code'
    @@Unit = 'unit'
    @@Object = 'object'
    @@Detail = 'detail'
    @@Location = 'location'
    
    def initialize
        @sql   = SQLite3::Database.new "./DefectModel.db3"
        @couch = CouchRest.database("http://127.0.0.1:5984/defect_model")
        @couch.create!
		begin
			@design = @couch.get '_design/' + @@Design
		rescue RestClient::ResourceNotFound
			@design = nil
		end
		@doc_id_by_type_and_sql_id = {} # gives us the couch doc._id for a (type, sql pk)-tuple
        @sqlid_for_docid = {}           # gives us the sql pk for a given couch doc._id
    end
    
    
    def rebuild_couch
        create_docs
        create_relations
        commit
        rebuild_views        
    end
    
    def commit
        reset_couch        
        bulk_response = @couch.bulk_save @docs
        if bulk_response.instance_of? Array
            for response in bulk_response
                unless response["ok"]
                    print  response["id"] + " " + response["error"] + " (" + response["reason"] + ")\n"
                end
            end
            true            
        end        
    end

    def create_relations
        create_relation_spaces_to_units
        
        create_relation_spaces_to_objects
        create_relation_spaces_units_to_objects
        
        create_relation_spaces_units_to_details
        create_relation_spaces_objects_to_details
        create_relation_spaces_units_objects_to_details

        create_relation_spaces_to_errorcodes
        create_relation_spaces_units_to_errorcodes
        create_relation_spaces_objects_to_errorcodes
        create_relation_spaces_units_objects_to_errorcodes
        create_relation_spaces_units_details_to_errorcodes
        create_relation_spaces_objects_details_to_errorcodes
        create_relation_spaces_units_objects_details_to_errorcodes
    end
    


    
    def create_docs
        # this creates the raw documents (stored in @docs), without any relations
        @docs = []
        sqlstr = "SELECT dm_SpaceName, dm_SpaceHasNrList, dm_SpaceIsOutdoors, dm_SpaceIsIndoors, " +
                "dm_SpaceID, dm_SpaceSortingPriority " +
                "FROM DM_Spaces;"
        @sql.execute sqlstr do |row|
        	doc_id = @@Space + "_" + row[0]
            @docs << { 'type'    => @@Space,
            			'_id'		=> doc_id,
                       'name'     =>  row[0],
                       'has_nrlist'   => (row[1] == 0 ? false : true), 
                       'is_indoors'  => (row[2] == 0 ? false : true),
                       'is_outdoors' => (row[3] == 0 ? false : true),
                       'sorting_priority' => row[5] }
            @doc_id_by_type_and_sql_id[[@@Space, row[4]]] = doc_id            
            @sqlid_for_docid[doc_id] = row[4]
        end
        
         sqlstr = "SELECT dm_UnitName, dm_UnitID, dm_UnitHasNrList, dm_UnitSortingPriority " +
         " FROM DM_Units;"
         @sql.execute sqlstr do |row|
         	doc_id = @@Unit + "_" + row[0]
             @docs << ( { 'type'  => @@Unit,
                        'name'   => row[0],
                        'has_nrlist' => (row[2] == 0 ? false : true ),
 						'_id'	  => doc_id,
 						'sorting_priority' => row[3] } )
			@doc_id_by_type_and_sql_id[[@@Unit, row[1]]] = doc_id
            @sqlid_for_docid[doc_id] = row[1]
         end
        
        sqlstr = "SELECT dm_ObjectName, dm_ObjectID, dm_ObjectHasNrList" +
                " FROM DM_Object;"
        @sql.execute sqlstr do |row|
            doc_id = @@Object + "_" + row[0]
            @docs << ( { 'type'  => @@Object,
                        'name'   => row[0],
                        'has_nrlist' => (row[2] == 0 ? false : true),
                        '_id'	  => doc_id} )
            @doc_id_by_type_and_sql_id[[@@Object, row[1]]] = doc_id
            @sqlid_for_docid[doc_id] = row[1]
        end        


        sqlstr = "SELECT dm_ErrorCodeID, dm_ErrorCodeName " +
                 "FROM DM_ErrorCodes;"
        @sql.execute sqlstr do |row|
            doc_id = @@ErrorCode + "_" + row[1]
            @docs << ( { 'type'  => @@ErrorCode,
                         'name'   => row[1],
  			 			'_id'	  => doc_id } )
            @doc_id_by_type_and_sql_id[[@@ErrorCode, row[0]]] = doc_id
            @sqlid_for_docid[doc_id] = row[1]
        end
     
     
        sqlstr = "SELECT dm_DetailName, dm_DetailID, dm_DetailHasNrList " +
                 "FROM DM_Details;"
        @sql.execute sqlstr do |row|
            doc_id = @@Detail + "_" + row[0]
            @docs << ( { 'type'  => @@Detail,
                      'name'   => row[0],
                      'has_nrlist' => (row[2] == 0 ? false : true),
                      '_id'	  => doc_id } )
            @doc_id_by_type_and_sql_id[[@@Detail, row[1]]] = doc_id
            @sqlid_for_docid[doc_id] = row[1]
        end
        
        sqlstr = "SELECT dm_LocationName, dm_LocationID " +
                 "FROM DM_Locations;"
 		@sql.execute sqlstr do |row|
            @docs << ( {  'type' => @@Location,
                      '_id'  => @@Location + "_" + row[0],
                      'name' =>  row[0] } )
        end
              
        true       
    end
    
        
    def rebuild_views
        # in order to verify the relation modeling, all relations are represented as a Javascript view
		begin
			@design = @couch.get '_design/' + @@Design
		rescue RestClient::ResourceNotFound
			@design = CouchRest::Design.new
			@design.name = @@Design
			@design.database = @couch
			@design['language'] = 'javascript'
		end

        @design['views'] = { 'units_by_space' => { 'map' =>
<<JAVASCRIPT
// this is the most basic example of the relationship
// views. It takes advantage of the fact that emit can
// be called multiple times for the same document. To
// get all units belonging to a given space, append
// ?key="space_FOO" to the URL when quering
function(doc) {
    if ( doc.type == "unit" )
        for each ( space in doc.spaces )
            emit(space, doc.name);
}
JAVASCRIPT
                        },'objects_by_space' => { 'map' =>
<<JAVASCRIPT
            function(doc) {
                if ( doc.type == "object" )
                    for each ( space in doc.spaces )
                        emit(space, doc.name);
            }
JAVASCRIPT
            },'objects_by_space_unit' => { 'map' =>
<<JAVASCRIPT
            function(doc) {
                if ( doc.type == "object" )
                    for ( space in doc.spaces_units )
                        for each (unit in doc.spaces_units[space] )
                            emit([space, unit], doc._id);
            }
JAVASCRIPT
                        }, 'details_by_space_unit' => { 'map' =>
<<JAVASCRIPT
            function(doc) {
                if ( doc.type == "detail" )
                    for ( space in doc.spaces_units )
                        for each (unit in doc.spaces_units[space] )
                            emit([space, unit], doc._id);
                            }
JAVASCRIPT
                            }, 'details_by_space_object' => { 'map' =>
<<JAVASCRIPT
            function(doc) {
                if ( doc.type == "detail" )
                    for ( space in doc.spaces_objects )
                        for each (object in doc.spaces_objects[space] )
                            emit([space, object], doc._id);
            }
JAVASCRIPT
                        }, 'details_by_space_unit_object' => { 'map' =>
<<JAVASCRIPT
            function(doc) {
                if ( doc.type == "detail" )
                    for ( space in doc.spaces_units_objects )
                        for ( unit in doc.spaces_units_objects[space] )
                            for each ( object in doc.spaces_units_objects[space][unit] )
                                emit([space, unit, object], doc._id);
                            }
JAVASCRIPT
                                                
                            }, 'error_codes_by_space' => { 'map' =>
<<JAVASCRIPT
                                    function( doc) {
                                        if ( doc.type == "error_code" )
                                            for each (space in doc.spaces)
                                                emit( space, doc.name);
                                    }
JAVASCRIPT
                            }, 'error_codes_by_space_unit' => { 'map' =>
<<JAVASCRIPT
                function( doc) {
                    if ( doc.type == "error_code" )
                        for ( space in doc.spaces_units)
                            for each ( unit in doc.spaces_units[space] )
                              emit( [space, unit], doc.name);
                    }
JAVASCRIPT
                            }, 'error_codes_by_space_object' => { 'map' =>
<<JAVASCRIPT
                function( doc) {
                    if ( doc.type == "error_code" )
                        for ( space in doc.spaces_objects)
                            for each ( object in doc.spaces_objects[space] )
                                emit( [space, object], doc.name);
                }
JAVASCRIPT
                                }, 'error_codes_by_space_unit_object' => { 'map' =>
<<JAVASCRIPT
                function( doc) {
                    if ( doc.type == "error_code" )
                        for ( space in doc.spaces_units_objects )
                            for ( unit in doc.spaces_units_objects[space] )
                                for each ( object in doc.spaces_units_objects[space][unit] )
                                    emit([space, unit, object], doc._id);
                }
JAVASCRIPT
                                }, 'error_codes_by_space_object_detail' => { 'map' =>
<<JAVASCRIPT
                function( doc) {
                    if ( doc.type == "error_code" )
                        for ( space in doc.spaces_objects_details )
                            for ( object in doc.spaces_objects_details[space] )
                                for each ( detail in doc.spaces_objects_details[space][object] )
                                    emit([space, object, detail], doc._id);
                }
JAVASCRIPT
                                }, 'error_codes_by_space_unit_detail' => { 'map' =>
<<JAVASCRIPT
                function( doc) {
                    if ( doc.type == "error_code" )
                        for ( space in doc.spaces_units_details )
                            for ( unit in doc.spaces_units_details[space] )
                                for each ( detail in doc.spaces_units_details[space][unit] )
                                    emit([space, unit, detail], doc._id);
                    }
JAVASCRIPT
                                }, 'error_codes_by_space_unit_object_detail' => { 'map' =>
<<JAVASCRIPT
                function( doc) {
                    if ( doc.type == "error_code" )
                        for ( space in doc.spaces_units_objects_details )
                            for ( unit in doc.spaces_units_objects_details[space] )
                                for ( object in doc.spaces_units_objects_details[space][unit] )
                                    for each ( detail in doc.spaces_units_objects_details[space][unit][object] )
                                        emit([space, unit, object, detail], doc._id);
                }
JAVASCRIPT
                                }, 'construction_faults' => { 'map' =>
<<JAVASCRIPT
                function( doc) {
                    if ( doc.type == "construction_fault" )
                            emit( doc.created_at, null );
                    }
JAVASCRIPT
                                }
                    }

        @design['filters'] = { 'test' =>
<<JAVASCRIPT
function(doc, req){
    return true;
}
JAVASCRIPT
        }
		@design.save					                   
    	true             
    end

    def reset_couch
        @couch.recreate!
        true
    end
      
    def  create_relation_spaces_to_units
        # this is one of the simple relations
        space_docids_for_unit_docid = {} # this hash stores an array with references to spaces for each unit
        last_unit_docid = nil
        sqlstr =
<<SQL
        SELECT DISTINCT dm_unitID, dm_spaceID FROM DM_DefectRelation
        WHERE dm_unitID IS NOT NULL AND dm_spaceID IS NOT NULL
        ORDER BY dm_unitID;
SQL
        @sql.execute sqlstr do |row|
            cur_unit_docid = @doc_id_by_type_and_sql_id[[@@Unit, row[0]]]
            cur_space_docid = @doc_id_by_type_and_sql_id[[@@Space, row[1]]]
            if last_unit_docid != cur_unit_docid                
                space_docids_for_unit_docid[cur_unit_docid] = []
            end
            space_docids_for_unit_docid[cur_unit_docid] << cur_space_docid
            last_unit_docid = cur_unit_docid
        end
                                
        @docs.each do |doc|
            if doc['type'] == @@Unit
                parent_spaces_docids = space_docids_for_unit_docid[doc['_id']]
                # add the space doc ids the unit belongs to
                if parent_spaces_docids.instance_of? Array
                    doc[@@Space + 's'] = parent_spaces_docids
                end
            end
        end
        space_docids_for_unit_docid                
    end

                
    def create_relation_spaces_to_objects
        space_docids_for_object_docid = {}
        last_object_docid = nil
        sqlstr =
<<SQL
        SELECT DISTINCT dm_objectID, dm_spaceID FROM DM_DefectRelation
        WHERE dm_objectID IS NOT NULL AND dm_spaceID IS NOT NULL
        AND dm_unitID IS NULL
        ORDER BY dm_objectID;
SQL
        @sql.execute sqlstr do |row|
            cur_object_docid = @doc_id_by_type_and_sql_id[[@@Object, row[0]]]
            cur_space_docid = @doc_id_by_type_and_sql_id[[@@Space, row[1]]]
            if last_object_docid != cur_object_docid
                space_docids_for_object_docid[cur_object_docid] = []
            end
            space_docids_for_object_docid[cur_object_docid] << cur_space_docid
            last_object_docid = cur_object_docid
        end
        @docs.each do |doc|
            if doc['type'] == @@Object
                parent_spaces_docids = space_docids_for_object_docid[doc['_id']]
                # add the space doc ids the object belongs to
                doc[@@Space + 's'] = parent_spaces_docids if parent_spaces_docids.instance_of? Array
            end
        end
        true
    end
    
    
    
    
    def create_relation_spaces_units_to_objects
        sqlstr =
<<SQL
        SELECT DISTINCT dm_objectID, dm_spaceID, dm_unitID
        FROM DM_DefectRelation
        WHERE dm_objectID IS NOT NULL AND dm_spaceID IS NOT NULL AND dm_unitID IS NOT NULL
        ORDER BY dm_objectID, dm_spaceID;
SQL
        last_object = nil
        last_space = nil
        spaces_units_by_object = {}
    
        @sql.execute sqlstr do |row|
            cur_object = @doc_id_by_type_and_sql_id[[@@Object, row[0]]]
            cur_space  = @doc_id_by_type_and_sql_id[[@@Space,  row[1]]]
            cur_unit   = @doc_id_by_type_and_sql_id[[@@Unit,   row[2]]]            
            if last_object != cur_object
                spaces_units_by_object[cur_object] = {}
            end
            if last_space != cur_space || last_object != cur_object
                spaces_units_by_object[cur_object][cur_space] = []
            end
            spaces_units_by_object[cur_object][cur_space] << cur_unit
            last_object = cur_object
            last_space  = cur_space
        end        
        @docs.each do |doc|
            if doc['type'] == @@Object
                cur_spaces_units = spaces_units_by_object[doc['_id']]
                if cur_spaces_units.instance_of? Hash
                    doc[@@Space+'s_'+@@Unit+'s'] = cur_spaces_units
                end
            end
        end
        
        spaces_units_by_object
    end
                
                
    def create_relation_spaces_units_to_details
        sqlstr =
<<SQL
        SELECT DISTINCT dm_detailID, dm_spaceID, dm_unitID  FROM DM_DefectRelation
        WHERE dm_ObjectID IS NULL
        AND dm_detailID IS NOT NULL
        AND dm_spaceID IS NOT NULL
        AND dm_unitID IS NOT NULL
        ORDER BY dm_detailID, dm_spaceID;
SQL
        last_detail = nil
        last_space = nil
        spaces_and_units_by_detail = {}
        @sql.execute sqlstr do |row|
            cur_detail = @doc_id_by_type_and_sql_id[[@@Detail, row[0]]]
            cur_space  = @doc_id_by_type_and_sql_id[[@@Space, row[1]]]
            cur_unit   = @doc_id_by_type_and_sql_id[[@@Unit, row[2]]]
            if cur_detail != last_detail
                spaces_and_units_by_detail[cur_detail] = {}
            end
            if last_space != cur_space || last_detail != cur_detail
                spaces_and_units_by_detail[cur_detail][cur_space] = []
            end
            spaces_and_units_by_detail[cur_detail][cur_space] << cur_unit
            last_detail = cur_detail
            last_space = cur_space
        end
        @docs.each do |doc|
            if doc['type'] == @@Detail
                parent_spaces_units = spaces_and_units_by_detail[doc['_id']]
                if parent_spaces_units.instance_of? Hash
                    doc[@@Space+'s_'+@@Unit+'s'] = parent_spaces_units
                end 
            end
        end                
        true
    end
                                
                
    def create_relation_spaces_objects_to_details
        sqlstr =
<<SQL
        SELECT DISTINCT dm_detailID, dm_spaceID, dm_objectID  FROM DM_DefectRelation
        WHERE dm_unitID IS NULL
        AND dm_detailID IS NOT NULL
        AND dm_spaceID IS NOT NULL
        AND  dm_ObjectID IS NOT NULL
        ORDER BY dm_detailID, dm_spaceID;
SQL
        last_detail = nil
        last_space = nil
        spaces_and_objects_by_detail = {}
        @sql.execute sqlstr do |row|
            cur_detail = @doc_id_by_type_and_sql_id[[@@Detail, row[0]]]
            cur_space  = @doc_id_by_type_and_sql_id[[@@Space, row[1]]]
            cur_object   = @doc_id_by_type_and_sql_id[[@@Object, row[2]]]
            if cur_detail != last_detail
                spaces_and_objects_by_detail[cur_detail] = {}
            end
            if last_space != cur_space || last_detail != cur_detail
                spaces_and_objects_by_detail[cur_detail][cur_space] = []
            end
            spaces_and_objects_by_detail[cur_detail][cur_space] << cur_object
            last_detail = cur_detail
            last_space = cur_space
        end
        @docs.each do |doc|
            if doc['type'] == @@Detail
                parent_spaces_objects = spaces_and_objects_by_detail[doc['_id']]
                if parent_spaces_objects.instance_of? Hash
                    doc[@@Space+'s_'+@@Object+'s'] = parent_spaces_objects
                end 
            end
        end                
        true
    end
       
    def create_relation_spaces_units_objects_to_details
        sqlstr =
<<SQL
        SELECT DISTINCT dm_detailID, dm_spaceID, dm_unitID, dm_objectID FROM DM_DefectRelation
        WHERE dm_unitID IS NOT NULL
        AND dm_detailID IS NOT NULL
        AND dm_spaceID IS NOT NULL
        AND  dm_ObjectID IS NOT NULL
        ORDER BY dm_detailID, dm_spaceID, dm_unitID;
SQL
        spaces_units_objects_by_detail = {}
        last_detail, last_space, last_unit = nil
        @sql.execute sqlstr do |row|
            cur_detail = @doc_id_by_type_and_sql_id[[@@Detail,  row[0]]]
            cur_space  = @doc_id_by_type_and_sql_id[[@@Space,   row[1]]]
            cur_unit = @doc_id_by_type_and_sql_id[[@@Unit,      row[2]]]
            cur_object = @doc_id_by_type_and_sql_id[[@@Object,  row[3]]]
            
            if cur_detail != last_detail
                spaces_units_objects_by_detail[cur_detail] = {}
            end
            if last_space != cur_space || cur_detail != last_detail
                spaces_units_objects_by_detail[cur_detail][cur_space] = {}
            end
            if last_unit != cur_unit || last_space != cur_space || cur_detail != last_detail
                spaces_units_objects_by_detail[cur_detail][cur_space][cur_unit] = []
            end
            
            spaces_units_objects_by_detail[cur_detail][cur_space][cur_unit] << cur_object
            
            last_detail = cur_detail
            last_space = cur_space
            last_unit = cur_unit
        end
        @docs.each do |doc|
            if doc['type'] == @@Detail
                parent_spaces_units_objects = spaces_units_objects_by_detail[doc['_id']]
                if parent_spaces_units_objects.instance_of? Hash
                    doc[@@Space+'s_'+@@Unit+'s_'+@@Object+'s'] = parent_spaces_units_objects
                end
            end
        end
        true 
    end

                
    def create_relation_spaces_to_errorcodes
        sqlstr =
<<SQL
        SELECT DISTINCT dm_ErrorCodeID, dm_spaceID FROM DM_DefectRelation
        WHERE dm_spaceID IS NOT NULL
        AND dm_unitID IS NULL
        AND dm_objectID IS NULL
        AND dm_DetailID IS NULL
        AND dm_ErrorCodeID IS NOT NULL
        ORDER BY dm_ErrorCodeID;
SQL
        last_errcode  = nil
        spaces_by_errcode = {}
        @sql.execute sqlstr do |row|
            cur_errcode = @doc_id_by_type_and_sql_id[[@@ErrorCode, row[0]]]
            cur_space = @doc_id_by_type_and_sql_id[[@@Space, row[1]]]
            if last_errcode != cur_errcode
                spaces_by_errcode[cur_errcode] = []
            end
            spaces_by_errcode[cur_errcode] << cur_space
            last_errcode = cur_errcode
        end
        @docs.each do |doc|
            if doc['type'] == @@ErrorCode
                parent_spaces = spaces_by_errcode[doc['_id']]
                doc[@@Space + 's'] = parent_spaces if parent_spaces.instance_of? Array
            end
        end
        true
    end
                                        
    def create_relation_spaces_units_to_errorcodes
        sqlstr =
<<SQL
        SELECT DISTINCT dm_ErrorCodeID, dm_spaceID, dm_UnitID FROM DM_DefectRelation
        WHERE dm_spaceID IS NOT NULL
        AND dm_unitID IS NOT NULL
        AND dm_objectID IS NULL
        AND dm_DetailID IS NULL
        AND dm_ErrorCodeID IS NOT NULL
        ORDER BY dm_ErrorCodeID, dm_spaceID;
SQL
        last_errcode, last_space = nil
        spaces_units_by_errcode = {}
                                        
        @sql.execute sqlstr do |row|
            cur_errcode = @doc_id_by_type_and_sql_id[[@@ErrorCode, row[0]]]
            cur_space = @doc_id_by_type_and_sql_id[[@@Space, row[1]]]
            cur_unit = @doc_id_by_type_and_sql_id[[@@Unit, row[2]]]
            if cur_errcode != last_errcode
                spaces_units_by_errcode[cur_errcode] = {}
            end
            if last_space != cur_space || last_errcode != cur_errcode
                spaces_units_by_errcode[cur_errcode][cur_space] = []
            end
            spaces_units_by_errcode[cur_errcode][cur_space] << cur_unit
            last_errcode = cur_errcode
            last_space = cur_space
        end
        @docs.each do |doc|
            if doc['type'] == @@ErrorCode
                parent_spaces_units = spaces_units_by_errcode[doc['_id']]
                if parent_spaces_units.instance_of? Hash
                    doc[@@Space+'s_'+@@Unit+'s'] = parent_spaces_units
                end 
            end
        end                
        true
    end
    
    
    def create_relation_spaces_objects_to_errorcodes
        sqlstr =
<<SQL
        SELECT DISTINCT dm_ErrorCodeID, dm_spaceID, dm_objectID FROM DM_DefectRelation
        WHERE dm_spaceID IS NOT NULL
        AND dm_unitID IS NULL
        AND dm_objectID IS NOT NULL
        AND dm_DetailID IS NULL
        AND dm_ErrorCodeID IS NOT NULL
        ORDER BY dm_ErrorCodeID, dm_spaceID;
SQL
        last_errcode, last_space = nil
        spaces_objects_by_errcode = {}
        
        @sql.execute sqlstr do |row|
            cur_errcode = @doc_id_by_type_and_sql_id[[@@ErrorCode, row[0]]]
            cur_space = @doc_id_by_type_and_sql_id[[@@Space, row[1]]]
            cur_object = @doc_id_by_type_and_sql_id[[@@Object, row[2]]]
            if cur_errcode != last_errcode
                spaces_objects_by_errcode[cur_errcode] = {}
            end
            if last_space != cur_space || last_errcode != cur_errcode
                spaces_objects_by_errcode[cur_errcode][cur_space] = []
            end
            spaces_objects_by_errcode[cur_errcode][cur_space] << cur_object
            last_errcode = cur_errcode
            last_space = cur_space
        end
        @docs.each do |doc|
            if doc['type'] == @@ErrorCode
                parent_spaces_objects = spaces_objects_by_errcode[doc['_id']]
                if parent_spaces_objects.instance_of? Hash
                    doc[@@Space+'s_'+@@Object+'s'] = parent_spaces_objects
                end 
            end
        end                
        true
    end
                    
                    
    def create_relation_spaces_units_objects_to_errorcodes
        sqlstr =
<<SQL
        SELECT DISTINCT dm_errorCodeID, dm_spaceID, dm_unitID, dm_objectID FROM DM_DefectRelation
        WHERE dm_unitID IS NOT NULL
        AND dm_detailID IS NOT NULL
        AND dm_spaceID IS NOT NULL
        AND  dm_ObjectID IS NOT NULL
        AND dm_DetailID IS NULL
        AND dm_errorCodeID IS NOT NULL
        ORDER BY dm_errorCodeID, dm_spaceID, dm_unitID;
SQL
        spaces_units_objects_by_errcode = {}
        last_errcode, last_space, last_unit = nil
        @sql.execute sqlstr do |row|
            cur_errcode = @doc_id_by_type_and_sql_id[[@@ErrorCode,  row[0]]]
            cur_space  = @doc_id_by_type_and_sql_id[[@@Space,   row[1]]]
            cur_unit = @doc_id_by_type_and_sql_id[[@@Unit,      row[2]]]
            cur_object = @doc_id_by_type_and_sql_id[[@@Object,  row[3]]]
            
            if cur_errcode != last_errcode
                spaces_units_objects_by_errcode[cur_errcode] = {}
            end
            if last_space != cur_space || cur_errcode != last_errcode
                spaces_units_objects_by_errcode[cur_errcode][cur_space] = {}
            end
            if last_unit != cur_unit || last_space != cur_space || cur_errcode != last_errcode
                spaces_units_objects_by_errcode[cur_errcode][cur_space][cur_unit] = []
            end
            
            spaces_units_objects_by_errcode[cur_errcode][cur_space][cur_unit] << cur_object
            
            last_errcode = cur_errcode
            last_space = cur_space
            last_unit = cur_unit
        end
        @docs.each do |doc|
            if doc['type'] == @@ErrorCode
                parent_spaces_units_objects = spaces_units_objects_by_errcode[doc['_id']]
                if parent_spaces_units_objects.instance_of? Hash
                    doc[@@Space+'s_'+@@Unit+'s_'+@@Object+'s'] = parent_spaces_units_objects
                end
            end
        end
        spaces_units_objects_by_errcode
    end
                
                    
    def create_relation_spaces_objects_details_to_errorcodes
        sqlstr =
<<SQL
        SELECT DISTINCT dm_errorCodeID, dm_spaceID, dm_objectID, dm_DetailID FROM DM_DefectRelation
        WHERE dm_unitID IS NULL
        AND dm_detailID IS NOT NULL
        AND dm_spaceID IS NOT NULL
        AND dm_ObjectID IS NOT NULL
        AND dm_DetailID IS NOT NULL
        AND dm_errorCodeID IS NOT NULL
        ORDER BY dm_errorCodeID, dm_spaceID, dm_objectID;
SQL
        spaces_objects_details_by_errcode = {}
        last_errcode, last_space, last_object = nil
        @sql.execute sqlstr do |row|
            cur_errcode = @doc_id_by_type_and_sql_id[[@@ErrorCode,  row[0]]]
            cur_space  = @doc_id_by_type_and_sql_id[[@@Space,   row[1]]]
            cur_object = @doc_id_by_type_and_sql_id[[@@Object,   row[2]]]
            cur_detail = @doc_id_by_type_and_sql_id[[@@Detail,  row[3]]]
            
            if cur_errcode != last_errcode
                spaces_objects_details_by_errcode[cur_errcode] = {}
            end
            if last_space != cur_space || cur_errcode != last_errcode
                spaces_objects_details_by_errcode[cur_errcode][cur_space] = {}
            end
            if last_object != cur_object || last_space != cur_space || cur_errcode != last_errcode
                spaces_objects_details_by_errcode[cur_errcode][cur_space][cur_object] = []
            end
            
            spaces_objects_details_by_errcode[cur_errcode][cur_space][cur_object] << cur_detail
            
            last_errcode = cur_errcode
            last_space = cur_space
            last_object = cur_object
        end
        @docs.each do |doc|
            if doc['type'] == @@ErrorCode
                parent_spaces_objects_details = spaces_objects_details_by_errcode[doc['_id']]
                if parent_spaces_objects_details.instance_of? Hash
                    doc[@@Space+'s_'+@@Object+'s_'+@@Detail+'s'] = parent_spaces_objects_details
                end
            end
        end
        spaces_objects_details_by_errcode
    end
                    
    def create_relation_spaces_units_details_to_errorcodes
        sqlstr =
<<SQL
        SELECT DISTINCT dm_errorCodeID, dm_spaceID, dm_unitID, dm_DetailID FROM DM_DefectRelation
        WHERE dm_unitID IS NOT NULL
        AND dm_detailID IS NOT NULL
        AND dm_spaceID IS NOT NULL
        AND dm_ObjectID IS  NULL
        AND dm_DetailID IS NOT NULL
        AND dm_errorCodeID IS NOT NULL
        ORDER BY dm_errorCodeID, dm_spaceID, dm_unitID;
SQL
        spaces_units_details_by_errcode = {}
        last_errcode, last_space, last_unit = nil
        @sql.execute sqlstr do |row|
            cur_errcode = @doc_id_by_type_and_sql_id[[@@ErrorCode,  row[0]]]
            cur_space  = @doc_id_by_type_and_sql_id[[@@Space,   row[1]]]
            cur_unit = @doc_id_by_type_and_sql_id[[@@Unit,   row[2]]]
            cur_detail = @doc_id_by_type_and_sql_id[[@@Detail,  row[3]]]
            
            if cur_errcode != last_errcode
                spaces_units_details_by_errcode[cur_errcode] = {}
            end
            if last_space != cur_space || cur_errcode != last_errcode
                spaces_units_details_by_errcode[cur_errcode][cur_space] = {}
            end
            if last_unit != cur_unit || last_space != cur_space || cur_errcode != last_errcode
                spaces_units_details_by_errcode[cur_errcode][cur_space][cur_unit] = []
            end
            
            spaces_units_details_by_errcode[cur_errcode][cur_space][cur_unit] << cur_detail
            
            last_errcode = cur_errcode
            last_space = cur_space
            last_unit = cur_unit
        end
        @docs.each do |doc|
            if doc['type'] == @@ErrorCode
                parent_spaces_units_details = spaces_units_details_by_errcode[doc['_id']]
                if parent_spaces_units_details.instance_of? Hash
                    doc[@@Space+'s_'+@@Unit+'s_'+@@Detail+'s'] = parent_spaces_units_details
                end
            end
        end
        spaces_units_details_by_errcode
    end
                    
    def create_relation_spaces_units_objects_details_to_errorcodes
    sqlstr =
<<SQL
    SELECT DISTINCT dm_errorCodeID, dm_spaceID, dm_unitID, dm_ObjectID, dm_DetailID FROM DM_DefectRelation
    WHERE dm_unitID IS NOT NULL
    AND dm_detailID IS NOT NULL
    AND dm_spaceID IS NOT NULL
    AND dm_ObjectID IS NOT NULL
    AND dm_DetailID IS NOT NULL
    AND dm_errorCodeID IS NOT NULL
    ORDER BY dm_errorCodeID, dm_spaceID, dm_unitID, dm_ObjectID;
SQL
    spaces_units_objects_details_by_errcode = {}
    last_errcode, last_space, last_unit, last_object = nil
    @sql.execute sqlstr do |row|
        cur_errcode = @doc_id_by_type_and_sql_id[[@@ErrorCode,  row[0]]]
        cur_space  = @doc_id_by_type_and_sql_id[[@@Space,   row[1]]]
        cur_unit = @doc_id_by_type_and_sql_id[[@@Unit,   row[2]]]
        cur_object = @doc_id_by_type_and_sql_id[[@@Object,   row[3]]]
        cur_detail = @doc_id_by_type_and_sql_id[[@@Detail,  row[4]]]
        
        if cur_errcode != last_errcode
            spaces_units_objects_details_by_errcode[cur_errcode] = {}
        end
        if last_space != cur_space || cur_errcode != last_errcode
            spaces_units_objects_details_by_errcode[cur_errcode][cur_space] = {}
        end
        if last_unit != cur_unit || last_space != cur_space || cur_errcode != last_errcode
            spaces_units_objects_details_by_errcode[cur_errcode][cur_space][cur_unit] = {}
        end
        if last_object != cur_object || last_unit != cur_unit || last_space != cur_space || cur_errcode != last_errcode
            spaces_units_objects_details_by_errcode[cur_errcode][cur_space][cur_unit][cur_object] = []
        end
        
        spaces_units_objects_details_by_errcode[cur_errcode][cur_space][cur_unit][cur_object] << cur_detail
        
        last_errcode = cur_errcode
        last_space = cur_space
        last_unit = cur_unit
        last_object = cur_object
    end
    @docs.each do |doc|
        if doc['type'] == @@ErrorCode
            parent_spaces_units_objects_details = spaces_units_objects_details_by_errcode[doc['_id']]
            if parent_spaces_units_objects_details.instance_of? Hash
                doc[@@Space+'s_'+@@Unit+'s_'+@@Object+'s_'+@@Detail+'s'] = parent_spaces_units_objects_details
            end
        end
    end
    spaces_units_objects_details_by_errcode
                    
    end 
end


unless $0['Migrator.rb'] == nil
    Migrator.new.rebuild_couch
end