Server demo (a CouchApp)
========================

Instructions:

1. Install the python 'couchapp' utility: https://github.com/couchapp/couchapp
2. The target db is configured as http://localhost:5984/defect_model. Change it in .couchapprc if neccessary
3. Run 'couchapp push' (this will upload the contents of this folder into the design document 'couchapp')

The app is available at http://localhost:5984/defect_model/_design/couchapp/index.html

Running the Migrator.rb will delete the whole database including the CouchApp, so you have to push it again afterwards.