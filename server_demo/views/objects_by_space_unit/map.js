function(doc) {
    if ( doc.type == "object" )
        for ( space in doc.spaces_units )
            for each (unit in doc.spaces_units[space] )
                emit([space, unit], doc.name);
}