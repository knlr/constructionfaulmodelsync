function(data) {
    var filteredRows = data.rows.filter( function( r ){
        return ( r.value.indexOf( $("#spaces_box").attr('value') ) < 0 );
    });
    return { rows : filteredRows };
}