function(){
    var unitToAdd =  $("#units_dropdown").attr('value');
    var selSpace = $("#spaces_box").attr('value');
    if ( selSpace.length  > 0 &&
         unitToAdd.length > 0 )
    {
        var db = $$($(this)).app.db;
        db.openDoc( unitToAdd,  { success : function(doc) {
            // add the selected space to doc.spaces
            doc.spaces.push( selSpace );
            // save the modfied doc to the db
            db.saveDoc( doc, { success : function() {
                //refresh the units box (TODO: listen to _changes instead)
                $("#units_box").trigger('spaceSelected');
            }});
        }});
    }
}