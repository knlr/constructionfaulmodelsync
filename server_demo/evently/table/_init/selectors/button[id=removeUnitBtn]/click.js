function(){
    var selUnit =  $("#units_box").attr('value');
    if ( selUnit.length > 0 ) {
        var selSpace = $("#spaces_box").attr('value');
        var db = $$($(this)).app.db;
        db.openDoc(selUnit, { success : function(doc) {
            // remove space from doc.spaces
            doc.spaces = doc.spaces.filter( function(sp){
                return sp != selSpace;
            });
            // save the modfied doc to the db
            db.saveDoc( doc, { success : function() {
                //refresh the units box (TODO: listen to _changes instead)
               // $("#units_box").trigger('spaceSelected');
            }});
        }});
    }
}